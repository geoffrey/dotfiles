# Geoffrey Frogeye's dotfiles

This repo holds most of my systems configuration.
It is built on top of the Nix ecosystem

## Directory structure

- `os`, `hm`, `nod`: Re-usable configuration for [NixOS](https://nixos.org/), [home-manager](https://github.com/nix-community/home-manager/#home-manager-using-nix), [Nix-on-Droid](https://github.com/nix-community/nix-on-droid#nix-on-droid) respectively
    - `<module-name>`: Module. Used to separate configuration in separate logical units.
        - `default.nix`: Entrypoint for that module. Contains Nix configuration for the module.
        - `<other file>`: Extra files: scripts to be installed, or ... whatever.
    - `default.nix`: Entrypoint for that system. Import all the modules.
    - `<other file>`: Files non-conforming to the structure above because I'm hurrying to have everything working before cleaning.
- `dk`: Re-usable configuration for [disko](https://github.com/nix-community/disko#disko---declarative-disk-partitioning)
    - `<name>.nix`: Partitionning scheme configuration. Don't assume a specific context, it will be imported as Disko and NixOS config.
- `options.nix`: Definition of options exposed to all systems (even though if not relevant for all, it's for simplicity sake).
- `<profile-name>` (e.g. `curacao`): Configurations for my different devices
    - `options.nix`: Common options configuration. Don't assume a specific context (for reasons unclear, I'm sure I can do something with that).
    - `hardware.nix`: NixOS configuration for that specific device.
    - `dk.nix`: Partitionning configuration. Import a top-level `dk` scheme, adding disks ids and other configuration required.
    - `os.nix`, `hm.nix`, `nod.nix`: Entrypoint for the device/system configuration. Imports the above files (last two only for NixOS) and contains configuration specific to this combination.
- `<profile-name>_<media>` (e.g. `pindakaas_sd`): Alternate configuration for a device. Used to test a configuration without altering the "main" one.
    - `options.nix`: Same as above. Can be a symlink.
    - `hardware.nix`: Same as above. Should be a symlink.
    - `dk.nix`: Same as above. Should not be a symlink.
    - `os.nix`, `hm.nix`, `nod.nix`: Same as above. Should not be a symlink.

## Scripts

They all have a `-h` flag.

## Extensions

There's some things I'm not putting in this public repo: work-related things, and other sensitive things that probably shouldn't be public.
Those are stored in extensions, i.e. repos with a similar structure to this one, and ultimately importing things from it.
This is why you might see options not being seemingly used.
