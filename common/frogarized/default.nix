{ config, ... }:
let
  # Use ./frogarized.py to generate
  # Vendored to prevent IFDs
  frogarized = rec {
    common = {
      author = "Geoffrey Frogeye (with work from Ethan Schoonover)";
      base08 = "#e0332e";
      base09 = "#cf4b15";
      base0A = "#bb8801";
      base0B = "#8d9800";
      base0C = "#1fa198";
      base0D = "#008dd1";
      base0E = "#5c73c4";
      base0F = "#d43982";
    };
    light = common // {
      base00 = "#fff0f1";
      base01 = "#fae2e3";
      base02 = "#99a08d";
      base03 = "#89947f";
      base04 = "#677d64";
      base05 = "#5a7058";
      base06 = "#143718";
      base07 = "#092c0e";
      scheme = "Frogarized Light";
      slug = "frogarized-light";
    };
    dark = common // {
      base00 = "#092c0e";
      base01 = "#143718";
      base02 = "#5a7058";
      base03 = "#677d64";
      base04 = "#89947f";
      base05 = "#99a08d";
      base06 = "#fae2e3";
      base07 = "#fff0f1";
      scheme = "Frogarized Dark";
      slug = "frogarized-dark";
    };
  };
in
{
  config = {
    stylix = {
      base16Scheme = frogarized.${config.stylix.polarity};
      # On purpose also enable without a DE because stylix complains otherwise
      image = builtins.fetchurl {
        url = "https://get.wallhere.com/photo/sunlight-abstract-minimalism-green-simple-circle-light-leaf-wave-material-line-wing-computer-wallpaper-font-close-up-macro-photography-124350.png";
        sha256 = "sha256:1zfq3f3v34i45mi72pkfqphm8kbhczsg260xjfl6dbydy91d7y93";
      };
    };
  };
}
