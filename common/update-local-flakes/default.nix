{ pkgs, ... }:
pkgs.writers.writePython3Bin "update-local-flakes" { } (builtins.readFile ./update-local-flakes.py)
