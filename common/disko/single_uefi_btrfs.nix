{
  config,
  ...
}:
let
  passwordFile = "/tmp/dotfiles_${config.frogeye.name}_password";
in
{
  disko.devices = {
    disk = {
      "${config.frogeye.name}" = {
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              # Needs enough to store multiple kernel generations
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [
                  "defaults"
                ];
              };
            };
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "${config.frogeye.name}";
                passwordFile = passwordFile;
                settings = {
                  # Not having SSDs die fast is more important than crypto
                  # nerds that could potentially discover which filesystem I
                  # use from TRIM patterns
                  allowDiscards = true;
                };
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ];
                  subvolumes = {
                    "/nixos" = {
                      mountpoint = "/";
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                    };
                    "/home" = {
                      mountpoint = "/home";
                      mountOptions = [
                        "compress=zstd"
                        "relatime"
                      ];
                    };
                    "/nix" = {
                      mountpoint = "/nix";
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                    };
                    # Maybe later
                    # "/swap" = {
                    #   mountpoint = "/.swapvol";
                    #   swap.swapfile.size = "20M";
                    # };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
