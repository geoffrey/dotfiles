{
  description = "Geoffrey Frogeye's base configurations";

  inputs = {
    # Packages
    nixpkgs.url = "nixpkgs/nixos-24.11";
    unixpkgs.url = "nixpkgs/master";
    # OS
    disko = {
      url = "disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "nixos-hardware";
    # NOD
    nix-on-droid = {
      url = "github:nix-community/nix-on-droid"; # No 24.11 yet
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
    # HM
    home-manager = {
      url = "home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix = {
      url = "github:danth/stylix/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixvim = {
      url = "github:nix-community/nixvim/nixos-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur.url = "github:nix-community/NUR";
    # Local
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs =
    {
      self,
      nixpkgs,
      unixpkgs,
      disko,
      nix-on-droid,
      flake-utils,
      nur,
      ...
    }@attrs:
    # Machine independant outputs
    let
      nixpkgsConfig = {
        config = {
          allowUnfree = true;
          # TODO Handpick exceptions
        };
        overlays = [
          (import ./common/update-local-flakes/overlay.nix)
          nur.overlays.default
          (
            # Cherry-pick packages from future
            self: super:
            let
              upkgs = import unixpkgs { inherit (super) system; };
            in
            {
              jjui = upkgs.jjui;
              labelle = upkgs.labelle;
              orca-slicer = upkgs.orca-slicer; # Not prebuilt in 24.11 for some reason
              nextcloud-client = upkgs.nextcloud-client; # Need https://github.com/nextcloud/desktop/pull/7714
            }
          )
        ];
      };
      homeManagerConfig = {
        sharedModules = [ self.homeManagerModules.dotfiles ];
        extraSpecialArgs = attrs;
      };
      lib = {
        nixosSystem =
          {
            system,
            modules ? [ ],
          }:
          nixpkgs.lib.nixosSystem {
            inherit system;
            specialArgs = attrs // {
              upkgs = import unixpkgs {
                inherit system;
              };
            };
            modules = modules ++ [
              self.nixosModules.dotfiles
              # nur.modules.nixos.default
              {
                nixpkgs = nixpkgsConfig;
                home-manager = homeManagerConfig;
                frogeye.toplevel = {
                  _type = "override";
                  content = self;
                  priority = 1000;
                };
              }
            ];
          };
        nixOnDroidConfiguration =
          {
            modules ? [ ],
          }:
          nix-on-droid.lib.nixOnDroidConfiguration {
            pkgs = import nixpkgs (
              nixpkgsConfig
              // {
                system = "aarch64-linux"; # nod doesn't support anything else
              }
            );
            modules = modules ++ [
              self.nixOnDroidModules.dotfiles
              {
                home-manager = homeManagerConfig;
              }
            ];
          };
        flakeTools =
          { self }:
          flake-utils.lib.eachDefaultSystem (
            system:
            let
              pkgs = import nixpkgs (
                nixpkgsConfig
                // {
                  inherit system;
                  # We do an overlay here so nixos-rebuild and other use lix.
                  # We don't do an overlay for the whole system because lix is not binary compatible.
                  overlays = [
                    (self: super: { nix = super.lix; })
                    (import ./common/update-local-flakes/overlay.nix)
                  ];
                }
              );
            in
            {
              apps = {
                disko = {
                  type = "app";
                  program = "${disko.packages.${system}.default}/bin/disko";
                };
                nixos-install = {
                  type = "app";
                  program = "${pkgs.nixos-install-tools}/bin/nixos-install";
                };
                nixos-rebuild = {
                  type = "app";
                  program = "${pkgs.nixos-rebuild}/bin/nixos-rebuild";
                };
                repl = {
                  type = "app";
                  program = "${pkgs.writeShellScript "vivarium-repl" ''
                    ${pkgs.lix}/bin/nix repl --expr 'let flake = builtins.getFlake "${self}"; in flake // flake.nixosConfigurations // rec { pkgs = import ${nixpkgs} {}; lib = pkgs.lib; }'
                  ''}";
                };
                # Available globally should this be needed in times of shenanigans
                updateLocalFlakes = {
                  type = "app";
                  program = "${pkgs.update-local-flakes}/bin/update-local-flakes";
                };
                nixosRebuild = {
                  type = "app";
                  program = "${pkgs.writeShellScript "rebuild" ''${
                    pkgs.writeShellApplication {
                      name = "rebuild";
                      runtimeInputs = with pkgs; [
                        nix-output-monitor
                        nixos-rebuild
                        jq
                      ];
                      text = builtins.readFile ./os/rebuild.sh;
                    }
                  }/bin/rebuild ${self} "$@"''}";
                };
              };
              formatter = pkgs.nixfmt-rfc-style;
            }
          );
      };
    in
    {
      # Reusable configurations
      inherit lib;
      nixosModules.dotfiles.imports = [ ./os ];
      nixOnDroidModules.dotfiles.imports = [ ./nod ];
      homeManagerModules.dotfiles.imports = [ ./hm ];
      # Actual configurations
      nixosConfigurations.curacao = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./curacao ];
      };
      nixosConfigurations.curacao-usb = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./curacao/usb.nix ];
      };
      nixosConfigurations.pindakaas = lib.nixosSystem {
        system = "aarch64-linux";
        modules = [ ./pindakaas ];
      };
      nixosConfigurations.pindakaas-sd = lib.nixosSystem {
        system = "aarch64-linux";
        modules = [ ./pindakaas/sd.nix ];
      };
      nixosConfigurations.cranberry = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./cranberry ];
      };
      nixOnDroidConfigurations.sprinkles = lib.nixOnDroidConfiguration { };
      # Fake systems
      nixosConfigurations.abavorana = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./abavorana/standin.nix ];
      };
      nixosConfigurations.ludwig = lib.nixosSystem {
        system = "x86_64-linux";
        modules = [ ./ludwig/standin.nix ];
      };
      nixosConfigurations.sprinkles = lib.nixosSystem {
        system = "aarch64-linux";
        modules = [ ./sprinkles/standin.nix ];
      };
      # TODO devices/ or configs/ folders
    }
    // (lib.flakeTools { inherit self; });
}
