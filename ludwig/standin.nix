{ ... }:
{
  config = {
    frogeye = {
      name = "ludwig";
      storageSize = "big";
      syncthing.name = "Ludwig";
    };
  };
}
