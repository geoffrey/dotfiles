{
  pkgs,
  lib,
  nixos-hardware,
  ...
}:
{
  config = {
    boot = {
      # nixos-hardware use latest kernel by default. It has been set a while ago, we maybe don't need it anymore?
      kernelPackages = pkgs.linuxPackages;

      # EFI Variables don't seem to work (no generation appear in systemd-boot with SD)
      loader.efi.canTouchEfiVariables = false;
    };

    frogeye.desktop = {
      x11_screens = [
        "DP-1"
        "eDP-1"
      ];
      maxVideoHeight = 1080;
      phasesCommands = {
        jour = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 3500
        '';
        crepuscule = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 3000
        '';
        nuit = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 700
        '';
      };
    };

    # Hardware decoding as suggested in link, works but doesn't seem to help much
    # (at least in dropped frames and perf stat output).
    # https://wiki.pine64.org/wiki/Mainline_Hardware_Decoding#mpv
    # Might be worth if using CI to build.

    home-manager.sharedModules = [
      {
        # gpu-hq is too much for it to handle, even with hw decoding
        config.programs.mpv.config.profile = lib.mkForce "default";
      }
    ];

    zramSwap = {
      # Not capable of building itself otherwise
      enable = true;
      memoryPercent = 150; # Factory settings
    };

    # Fixes black font on Alacritty
    environment.variables.PAN_MESA_DEBUG = "gl3";
  };
  imports = [
    nixos-hardware.nixosModules.pine64-pinebook-pro
  ];
}
