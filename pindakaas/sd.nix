{
  config,
  ...
}:
{
  config = {
    boot.loader.efi.canTouchEfiVariables = false;
    disko.devices.disk."${config.frogeye.name}".device = "/dev/disk/by-id/mmc-SN32G_0xfb19ae99";
    frogeye.name = "pindakaas-sd";
  };
  imports = [
    ../common/disko/single_uefi_btrfs.nix
    ./features.nix
    ./hardware.nix
  ];
}
