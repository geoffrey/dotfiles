{
  config,
  ...
}:
{
  config = {
    disko.devices.disk."${config.frogeye.name}".device = "/dev/disk/by-id/mmc-DA4064_0x931f080f";
    frogeye.name = "pindakaas";
  };
  imports = [
    ../common/disko/single_uefi_btrfs.nix
    ./hardware.nix
    ./features.nix
  ];
}
