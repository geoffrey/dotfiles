{ ... }:
{
  config = {
    frogeye = {
      name = "abavorana";
      storageSize = "big";
      syncthing.name = "Abavorana";
    };
  };
}
