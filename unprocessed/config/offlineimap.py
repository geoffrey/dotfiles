#! /usr/bin/env python2
from subprocess import check_output


def get_pass(account):
    return check_output("pass " + account, shell=True).splitlines()[0]

def beep():
    check_output("play -n synth sine E4 sine A5 remix 1-2 fade 0.5 1.2 0.5 2", shell=True)

