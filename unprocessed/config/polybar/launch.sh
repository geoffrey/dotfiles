#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Getting some vars
primary=$(xrandr | grep primary | head -1 | cut -d' ' -f1)
export ethI=$(/bin/ls /sys/class/net/ | grep ^enp* | head -1)
export wlanI=$(/bin/ls /sys/class/net/ | grep ^wl* | head -1)

# Launch bar for each display
polybar -m | cut -d':' -f1 | while read display
do
    export display=$display
    if [ "$display" == "$primary" ]
    then
        bar="primary"
    else
        bar="secondary"
    fi
    polybar -q $bar -c ~/.config/polybar/config.ini &
done

echo "Bars launched..."

