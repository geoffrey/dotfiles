{
  config,
  ...
}:
{
  config = {
    frogeye.name = "cranberry";
    disko.devices.disk."${config.frogeye.name
    }".device = "/dev/disk/by-id/nvme-UMIS_RPJTJ128MEE1MWX_SS0L25188X3RC12121TP";
  };
  imports = [
    ../common/disko/single_uefi_btrfs.nix
    ./hardware.nix
    ./features.nix
  ];
}
