{ ... }:
{
  config = {
    frogeye = {
      desktop.xorg = true;
      dev = {
        "3d" = true;
      };
      extra = true;
    };
  };
}
