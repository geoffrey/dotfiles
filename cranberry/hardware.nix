{
  pkgs,
  nixos-hardware,
  ...
}:
{
  config = {
    boot = {
      # From nixos-generate-config
      initrd.availableKernelModules = [
        "nvme"
        "xhci_pci"
        "usb_storage"
        "sd_mod"
        "sdhci_pci"
      ];
      kernelModules = [ "kvm-amd" ];
      # Times tpm2.target times out waiting for /dev/tpmrm0,
      # it's fine after unlocking though
      initrd.systemd.tpm2.enable = false;
    };

    # Needed for Wi-Fi
    hardware.enableRedistributableFirmware = true;

    frogeye.desktop = {
      x11_screens = [ "eDP-1" ];
      maxVideoHeight = 1080;

      phasesCommands = {
        jour = ''
          echo 0 | sudo tee /sys/class/leds/chromeos::kbd_backlight/brightness &
          ${pkgs.brightnessctl}/bin/brightnessctl set 30% &
        '';
        crepuscule = ''
          echo 1 | sudo tee /sys/class/leds/chromeos::kbd_backlight/brightness &
          ${pkgs.brightnessctl}/bin/brightnessctl set 10% &
        '';
        nuit = ''
          echo 10 | sudo tee /sys/class/leds/chromeos::kbd_backlight/brightness &
          ${pkgs.brightnessctl}/bin/brightnessctl set 0% &
        '';
      };
    };

    # Alt key swallowed the Meta one
    home-manager.users.geoffrey =
      { ... }:
      {
        xsession.windowManager.i3.config.modifier = "Mod1";
      };

    # 8 makes it run out of memory when rebuilding.
    nix.settings.max-jobs = 1;

  };
  imports = [
    nixos-hardware.nixosModules.common-cpu-amd
    nixos-hardware.nixosModules.common-gpu-amd
    nixos-hardware.nixosModules.common-pc-laptop
    nixos-hardware.nixosModules.common-pc-ssd
  ];
}
