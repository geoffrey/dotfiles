{
  pkgs,
  lib,
  nixos-hardware,
  ...
}:
let
  displays = {
    embedded = {
      output = "eDP-1";
      edid = "00ffffffffffff000dae381700000000011c01049526157802a155a556519d280b505400000001010101010101010101010101010101b43b804a71383440302035007dd61000001ac32f804a71383440302035007dd61000001a000000fe003059395747803137334843450a00000000000041319e001000000a010a2020004f";
    };
    deskLeft = {
      output = "HDMI-1-3"; # Internal HDMI port
      edid = "00ffffffffffff004c2d7b09333032302f160103803420782a01f1a257529f270a505423080081c0810081809500a9c0b300d1c00101283c80a070b023403020360006442100001a000000fd00353f1e5111000a202020202020000000fc00533234423432300a2020202020000000ff0048344d434230333533340a2020010702010400023a80d072382d40102c458006442100001e011d007251d01e206e28550006442100001e011d00bc52d01e20b828554006442100001e8c0ad090204031200c4055000644210000188c0ad08a20e02d10103e9600064421000018000000000000000000000000000000000000000000000000000000000000000000d2";
    };
    deskRight = {
      output = "DVI-I-2-1"; # DisplayLink
      edid = "00ffffffffffff004c2d7b093330323020160103803420782a01f1a257529f270a505423080081c0810081809500a9c0b300d1c00101283c80a070b023403020360006442100001a000000fd00353f1e5111000a202020202020000000fc00533234423432300a2020202020000000ff0048344d433830303836350a2020011c02010400023a80d072382d40102c458006442100001e011d007251d01e206e28550006442100001e011d00bc52d01e20b828554006442100001e8c0ad090204031200c4055000644210000188c0ad08a20e02d10103e9600064421000018000000000000000000000000000000000000000000000000000000000000000000d2";
    };
  };
in
{
  config = {
    boot = {
      # From nixos-generate-config
      initrd.availableKernelModules = [
        "xhci_pci"
        "ahci"
        "nvme"
        "usbhid"
        "sd_mod"
        "rtsx_usb_sdmmc"
      ];
      kernelModules = [ "kvm-intel" ];

      # UEFI works here, and variables can be touched
      loader = {
        efi.canTouchEfiVariables = lib.mkDefault true;
        grub = {
          enable = true;
          efiSupport = true;
          device = "nodev"; # Don't install on MBR
          # TODO Maybe we could? In case the HDD doesn't boot anymore?
        };
      };
    };
    # Also from nixos-generate-config
    hardware.enableRedistributableFirmware = true;
    # TODO Do we really need that? Besides maybe microcode?

    # AnnePro 2
    hardware.keyboard.qmk.enable = true;

    frogeye.desktop = {
      x11_screens = [
        displays.deskLeft.output
        displays.deskRight.output
      ];
      maxVideoHeight = 1440;
      numlock = true;
      phasesCommands = {
        jour = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 40000 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 20 -d 1 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 20 -d 2 &
        '';
        crepuscule = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 10000 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 10 -d 1 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 10 -d 2 &
        '';
        nuit = ''
          ${pkgs.brightnessctl}/bin/brightnessctl set 1 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 0 -d 1 &
          ${pkgs.ddcutil}/bin/ddcutil setvcp 10 0 -d 2 &
        '';
        # TODO Display 2 doesn't work anymore?
      };
    };
    services = {
      autorandr = {
        profiles = {
          portable = {
            fingerprint.${displays.embedded.output} = displays.embedded.edid;
            config.${displays.embedded.output} = { };
          };
          extOnly = {
            fingerprint = {
              ${displays.embedded.output} = displays.embedded.edid;
              ${displays.deskLeft.output} = displays.deskLeft.edid;
              ${displays.deskRight.output} = displays.deskRight.edid;
            };
            config = {
              ${displays.embedded.output}.enable = false;
              ${displays.deskLeft.output} = {
                primary = true;
                mode = "1920x1200";
                rate = "59.95";
                position = "0x0";
              };
              ${displays.deskRight.output} = {
                mode = "1920x1200";
                rate = "59.95";
                position = "1920x0";
              };
            };
          };
          # TODO leftOnly and other things.Might want to abstract a few things first.
        };
      };
      # Needs prefetched binary blobs, see https://nixos.wiki/wiki/Displaylink
      xserver.videoDrivers = [
        "displaylink"
        "modesetting"
      ];
      # TODO See if nvidia and DL can work together.
    };
  };
  imports = [
    nixos-hardware.nixosModules.dell-g3-3779
  ];
}
