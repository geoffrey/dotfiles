{
  config,
  ...
}:
{
  config = {
    boot.loader.efi.canTouchEfiVariables = false;
    disko.devices.disk."${config.frogeye.name
    }".device = "/dev/disk/by-id/usb-Kingston_DataTraveler_3.0_E0D55EA57414F510489F0F1A-0:0";
    frogeye.name = "curacao-usb";
  };
  imports = [
    ../common/disko/single_uefi_btrfs.nix
    ./features.nix
    ./hardware.nix
  ];
}
