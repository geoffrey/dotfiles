{
  ...
}:
{
  config = {
    services.beesd.filesystems = {
      razmo = {
        spec = "/mnt/razmo";
        hashTableSizeMB = 512; # Recommended for 1 TiB, ×2 for compression, x2 for time
        extraOptions = [
          "--loadavg-target"
          "7.5"
        ];
      };
      rapido = {
        spec = "/mnt/rapido";
        hashTableSizeMB = 128; # 4 times smaller disk, 4 times smaller hashtable?
        extraOptions = [
          "--loadavg-target"
          "5"
        ];
      };
    };
  };
}
