{ pkgs, lib, ... }:
# MANU Snapper is not able to create the snapshot directory, so you'll need to do this after eventually running the backup script:
# sudo btrfs subvol create /mnt/razmo/$subvolume/.snapshots
let
  backup_subvolumes = [
    "nixos"
    "home.rapido"
    "home.nixos"
  ];
  backup_app = pkgs.writeShellApplication {
    name = "backup-subvolume";
    runtimeInputs = with pkgs; [
      coreutils
      btrfs-progs
    ];
    text = builtins.readFile ./backup.sh;
  };
  snapper_subvolumes = [
    "nixos"
    "home.rapido"
    "home.razmo"
    "home.nixos"
  ];
in
{
  services =
    let
      default = {
        # filesystem type
        FSTYPE = "btrfs";

        # run daily number cleanup
        NUMBER_CLEANUP = true;
        NUMBER_MIN_AGE = 1800;
        NUMBER_LIMIT = 25;

        # create hourly snapshots
        TIMELINE_CREATE = true;

        # cleanup hourly snapshots after some time
        TIMELINE_CLEANUP = true;
        TIMELINE_MIN_AGE = 1800;
        TIMELINE_LIMIT_HOURLY = "24";
        TIMELINE_LIMIT_DAILY = "31";
        TIMELINE_LIMIT_WEEKLY = "8";
        TIMELINE_LIMIT_MONTHLY = "0";
        TIMELINE_LIMIT_YEARLY = "0";

        # cleanup empty pre-post-pairs
        EMPTY_PRE_POST_CLEANUP = true;
      };
    in
    {
      snapper.configs = lib.attrsets.mergeAttrsList (
        map (s: {
          "${s}" = default // {
            SUBVOLUME = "/mnt/razmo/${s}";
          };
        }) snapper_subvolumes
      );
    };

  systemd = {
    services.bkp_rapido = {
      description = "Make a snapshot of the SSD to the HDD";
      before = [ "snapper-timeline.service" ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = map (s: "${backup_app}/bin/backup-subvolume ${s}") backup_subvolumes;
      };
      # TODO Harden
    };
    timers.bkp_rapido = {
      description = "Regular snapshot of SSD to HDD";
      timerConfig = {
        OnCalendar = "hourly";
      };
      wantedBy = [ "timers.target" ];
    };
  };
}
