{
  pkgs,
  lib,
  ...
}:
let
  zytemp_mqtt_src = pkgs.fetchFromGitHub {
    # owner = "patrislav1";
    owner = "GeoffreyFrogeye";
    repo = "zytemp_mqtt";
    rev = "push-nurpouorqoyr"; # Humidity + availability support
    sha256 = "sha256-nOhyBAgvjeQh9ys3cBJOVR67SDs96zBzxIRGpaq4yoA=";
  };
  zytemp_mqtt = pkgs.python3Packages.buildPythonPackage {
    name = "zytemp_mqtt";
    src = zytemp_mqtt_src;
    propagatedBuildInputs = with pkgs.python3Packages; [
      hidapi
      paho-mqtt
      pyaml
    ];
  };
  usb_zytemp_udev = pkgs.stdenv.mkDerivation {
    pname = "usb-zytemp-udev-rules";
    version = "unstable-2023-05-24";
    src = zytemp_mqtt_src;

    dontConfigure = true;
    dontBuild = true;
    dontFixup = true;

    installPhase = ''
      mkdir -p $out/lib/udev/rules.d
      cp udev/90-usb-zytemp-permissions.rules $out/lib/udev/rules.d/90-usb-zytemp.rules
      sed -i 's|"usb"|"hidraw"|' $out/lib/udev/rules.d/90-usb-zytemp.rules
    '';
  };
  mqtt_host = "192.168.7.53"; # Ludwig
in
{
  config = {
    environment.etc."zytempmqtt/config.yaml".text = lib.generators.toYAML { } {
      decrypt = true;
      mqtt_host = mqtt_host;
      friendly_name = "Desk sensor";
    };
    services.udev.packages = [ usb_zytemp_udev ];
    systemd = {
      services.zytemp_mqtt = {
        description = "Forward zyTemp CO2 sensor to MQTT";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          ExecStart = "${zytemp_mqtt}/bin/zytempmqtt";

          # Hardening (hapazardeous)
          CapabilityBoundingSet = "";
          DynamicUser = true;
          LockPersonality = true;
          MemoryDenyWriteExecute = false;
          NoNewPrivileges = true;
          PrivateTmp = true;
          PrivateUsers = true;
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHome = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          RemoveIPC = true;
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          SystemCallArchitectures = "native";
          SystemCallFilter = [
            "@system-service"
            "~@privileged"
            "~@resources"
          ];
          UMask = "0077";
        };
      };
    };
  };
}
