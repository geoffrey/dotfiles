{
  ...
}:
{
  config = {
    frogeye.name = "curacao";
  };
  imports = [
    ./backup
    ./co2meter
    ./dedup
    ./desk
    ./disko.nix
    ./features.nix
    ./hardware.nix
    ./homeautomation
    ./webcam
  ];
}
