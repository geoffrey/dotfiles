{
  config,
  ...
}:
# TODO Find a way to use keys in filesystem
# TODO Not relatime everywhere, thank you
# TODO Default options
let
  btrfs_args_ssd = [
    "rw"
    "relatime"
    "compress=zstd:3"
    "space_cache"
    "ssd"
  ];
  passwordFile = "/tmp/dotfiles_${config.frogeye.name}_password";
in
{
  disko.devices = {
    disk = {
      razmo = {
        type = "disk";
        device = "/dev/disk/by-id/ata-SDLF1DAR-960G-1HA1_A027C1A3";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              # Needs enough to store multiple kernel generations
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [
                  "defaults"
                ];
              };
            };
            swap = {
              size = "8G";
              content = {
                type = "swap";
                randomEncryption = true;
                # TODO NixOS documentation says not to use this with
                # hibernation, as it can't set the partition where the
                # hibernation image is saved. That's what I'm doing with Arch,
                # but I'm setting resume=, should test if it actually works?
                # Untranslated options from /etc/crypttab: swap,cipher=aes-xts-plain64,size=256
              };
            };
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "razmo";
                passwordFile = passwordFile;
                settings = {
                  allowDiscards = true;
                };
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ];
                  mountpoint = "/mnt/razmo";
                  subvolumes = {
                    "home.razmo" = {
                      mountpoint = "/home.heavy";
                      mountOptions = [
                        "compress=zstd"
                        "relatime"
                      ];
                    };
                    "steam" = {
                      mountpoint = "/opt/steam.razmo";
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                    };
                  };
                };
              };
            };
          };
        };
      };
      rapido = {
        type = "disk";
        device = "/dev/disk/by-id/nvme-GIGABYTE_GP-GSM2NE3256GNTD_SN204508906665";
        content = {
          type = "gpt";
          partitions = {
            swap = {
              priority = 10;
              start = "2048";
              size = "8G";
              type = "8200"; # Linux swap
              content = {
                type = "luks";
                name = "rapswap";
                passwordFile = passwordFile;
                settings = {
                  # keyFile = "/etc/keys/rapswap";
                  allowDiscards = true;
                };
                content = {
                  type = "swap";
                  resumeDevice = true;
                  # Untranslated options from /etc/fstab: defaults,pri=100
                };
              };
            };
            main = {
              priority = 20;
              content = {
                type = "luks";
                name = "rapido";
                initrdUnlock = true;
                passwordFile = passwordFile;
                settings = {
                  # keyFile = "/etc/keys/rapido";
                  allowDiscards = true;
                };
                content = {
                  type = "btrfs";
                  # extraArgs = [ "-f" ];
                  mountpoint = "/mnt/rapido";
                  mountOptions = btrfs_args_ssd;
                  subvolumes = {
                    # Should be temporary, to make sure we can revert to Arch anytime
                    "home.nixos" = {
                      mountpoint = "/home";
                      mountOptions = btrfs_args_ssd;
                    };
                    "home.rapido" = {
                      mountpoint = "/mnt/old/home";
                      mountOptions = btrfs_args_ssd;
                    };
                    nix = {
                      mountpoint = "/nix";
                      mountOptions = btrfs_args_ssd;
                    };
                    nixos = {
                      mountpoint = "/";
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
  services.btrfs.autoScrub = {
    enable = true;
    fileSystems = [
      "/mnt/razmo"
      "/mnt/rapido"
    ];
    # TODO Should be generable from disko config, right?
  };
}
