{ ... }:
{
  frogeye = {
    desktop = {
      xorg = true;
    };
    dev = {
      "3d" = true;
      c = true;
      docker = true;
      vm = true;
    };
    extra = true;
    gaming = true;
    storageSize = "big";
  };
}
