{
  pkgs,
  ...
}:
let
  desk_mqtt = pkgs.writers.writePython3 "desk_mqtt" {
    libraries = with pkgs.python3Packages; [
      pyusb
      ha-mqtt-discoverable
    ];
  } (builtins.readFile ./desk_mqtt.py);
  usb2lin06_udev = pkgs.writeTextFile {
    name = "usb2lin06-udev-rules";
    text = ''
      SUBSYSTEM=="usb", ATTR{idVendor}=="12d3", ATTR{idProduct}=="0002", MODE="0666"
    '';
    destination = "/lib/udev/rules.d/90-usb2lin06.rules";
  };
in
{
  config = {
    services.udev.packages = [ usb2lin06_udev ];
    systemd = {
      services.desk_mqtt = {
        description = "Control desk height via MQTT";
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          ExecStart = "${desk_mqtt}";
          RestartSec = 10;
          Restart = "on-failure";

          # Hardening (hapazardeous)
          CapabilityBoundingSet = "";
          DynamicUser = true;
          LockPersonality = true;
          MemoryDenyWriteExecute = false;
          NoNewPrivileges = true;
          PrivateTmp = true;
          PrivateUsers = true;
          ProtectClock = true;
          ProtectControlGroups = true;
          ProtectHome = true;
          ProtectHostname = true;
          ProtectKernelLogs = true;
          ProtectKernelModules = true;
          RemoveIPC = true;
          RestrictNamespaces = true;
          RestrictRealtime = true;
          RestrictSUIDSGID = true;
          SystemCallArchitectures = "native";
          SystemCallFilter = [
            "@system-service"
            "~@privileged"
            "~@resources"
          ];
          UMask = "0077";
        };
      };
    };
  };
}
