{
  pkgs,
  ...
}:
{
  config = {
    # TODO This should install cameractrls, but it seems like it's not easy to install.
    # In the meantime, we install Flatpak and do:
    # flatpak run hu.irl.cameractrls
    services.flatpak.enable = true;
    xdg.portal = {
      config.common.default = "*";
      enable = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
  };
}
