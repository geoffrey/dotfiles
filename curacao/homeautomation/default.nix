{
  ...
}:
{
  config = {
    networking = {
      # Allow mpd control from home assistant and phone
      firewall.extraCommands = ''
        iptables -A nixos-fw -p tcp -m tcp --dport 6600 -s 192.168.7.53 -j nixos-fw-accept
        iptables -A nixos-fw -p tcp -m tcp --dport 6600 -s 192.168.7.92 -j nixos-fw-accept
      '';
      interfaces.enp3s0.wakeOnLan.enable = true;
    };
    services.tlp.settings.WOL_DISABLE = false;
  };
}
