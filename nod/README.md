
# Flake example

`~/.config/nix-on-droid/flake.nix`

```nix
{
  description = "Nix-on-droid configuration for Sprinkles";

  inputs.dotfiles.url = "git+file:/data/data/com.termux.nix/files/home/.config/dotfiles";

  outputs = { dotfiles, ... }: {
    nixOnDroidConfigurations.default = dotfiles.nixOnDroidConfigurations.sprinkles;
  };
}
```
