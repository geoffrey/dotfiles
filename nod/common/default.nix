{ pkgs, config, ... }:
{
  environment.motd = "";
  home-manager = {
    useGlobalPkgs = true;
    config =
      { ... }:
      {
        frogeye = config.frogeye;
        home.file = {
          ".ssh/authorized_keys" = {
            # TODO Make an option and reuse at other places
            text = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPE41gxrO8oZ5n3saapSwZDViOQphm6RzqgsBUyA88pU geoffrey@frogeye.fr";
          };
        };
      };
  };
  system.stateVersion = "24.11";
  terminal.font = "${
    pkgs.nerdfonts.override {
      fonts = [ "DejaVuSansMono" ];
    }
  }/share/fonts/truetype/NerdFonts/DejaVuSansMNerdFont-Regular.ttf";
  time.timeZone = "Europe/Amsterdam";
}
