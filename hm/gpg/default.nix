{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.programs.gpg.enable {
    frogeye.hooks.lock = ''
      echo RELOADAGENT | ${pkgs.gnupg}/bin/gpg-connect-agent
    '';
    programs.gpg = {
      homedir = "${config.xdg.stateHome}/gnupg";
      settings = {
        # Remove fluff
        no-greeting = true;
        no-emit-version = true;
        no-comments = true;
        # Output format that I prefer
        keyid-format = "0xlong";
        # Show fingerprints
        with-fingerprint = true;
        # Make sure to show if key is invalid
        # (should be default on most platform,
        # but just to be sure)
        list-options = "show-uid-validity";
        verify-options = "show-uid-validity";
        # Stronger algorithm (https://wiki.archlinux.org/title/GnuPG#Different_algorithm)
        personal-digest-preferences = "SHA512";
        cert-digest-algo = "SHA512";
        default-preference-list = "SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed";
        personal-cipher-preferences = "TWOFISH CAMELLIA256 AES 3DES";
      };
      publicKeys = [
        {
          # Always install my own public key
          source = builtins.fetchurl {
            url = "https://keys.openpgp.org/vks/v1/by-fingerprint/4FBA930D314A03215E2CDB0A8312C8CAC1BAC289";
            sha256 = "sha256:10y9xqcy1vyk2p8baay14p3vwdnlwynk0fvfbika65hz2z8yw2cm";
          };
          trust = "ultimate";
        }
      ];
    };
    services.gpg-agent = rec {
      enableBashIntegration = true;
      enableZshIntegration = true;
      # gnome3 is nicer, but requires gcr as a dbus package.
      # Which is in my NixOS config, and on non-NixOS too.
      # It will fall back to ncurses when running in non-graphics mode.
      pinentryPackage = pkgs.pinentry-gnome3;
      # If inactive, the key will be forgotten after this time
      defaultCacheTtl = 3600;
      defaultCacheTtlSsh = defaultCacheTtl;
      # If active, the key will be forgotten adfter this time
      maxCacheTtl = 3 * 3600;
      maxCacheTtlSsh = maxCacheTtl;
    };
  };
}
