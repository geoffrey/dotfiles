{
  pkgs,
  ...
}:
{
  config = {
    frogeye.hooks.lock = ''
      ${pkgs.openssh}/bin/ssh-add -D
    '';
    programs.ssh = {
      enable = true;
      controlMaster = "auto";
      controlPersist = "60s"; # TODO Default is 10minutes... makes more sense no?
      # Ping the server frequently enough so it doesn't think we left (non-spoofable)
      serverAliveInterval = 30;
      matchBlocks."*" = {
        # Do not forward the agent (-A) to a machine by default,
        # as it is kinda a security concern
        forwardAgent = false;
        # Restrict terminal features (servers don't necessarily have the terminfo for my cutting edge terminal)
        sendEnv = [ "!TERM" ];
        # TODO Why not TERM=xterm-256color?
        extraOptions = {
          # Check SSHFP records
          VerifyHostKeyDNS = "yes";
        };
      };
    };
  };
}
