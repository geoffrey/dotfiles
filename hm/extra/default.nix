{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.extra {
    programs = {
      pandoc.enable = true;
      yt-dlp = {
        enable = true;
        settings = {
          format = "bestvideo[height<=${builtins.toString config.frogeye.desktop.maxVideoHeight}]+bestaudio/best";
          sponsorblock-mark = "all";
          sponsorblock-remove = "intro,outro,sponsor,selfpromo,preview,interaction,music_offtopic";
          sub-langs = "en,fr";
          write-auto-subs = true;
          write-subs = true;
          mark-watched = true; # Give something to creators, maybe
        };
      };
    };
    home.packages =
      with pkgs;
      (
        [
          # android tools
          android-tools

          # Communication
          signal-desktop
          (pkgs.callPackage ./whisperx.nix { }) # Transcribe voice messages

          # downloading
          # transmission TODO Collision if both transmissions are active?

          # Multimedia toolbox
          ffmpeg

          # documents
          visidata
          pdftk
          pdfgrep

          # Misc
          haskellPackages.dice

        ]
        ++ lib.optionals config.frogeye.desktop.xorg [

          # multimedia editors
          puddletag
          xournalpp

          # downloading
          transmission_4-qt

          # Misc
          rustdesk-flutter
        ]
      );
  };
}
