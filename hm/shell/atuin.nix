{
  ...
}:
{
  config = {
    programs.atuin = {
      enable = true;
      enableBashIntegration = true;
      enableZshIntegration = true;
      settings = {
        auto_sync = true;
        sync_frequency = "5m";
        sync_address = "https://atuin.frogeye.fr";
        ctrl_n_shortcuts = true; # Cranberry uses Alt for i3
        keymap_mode = "vim-insert";
        enter_accept = true;
        search_mode_shell_up_key_binding = "prefix";
        show_tabs = false;
        inline_height = 25;
      };
      flags = [
        "--disable-up-arrow"
      ];
    };
  };
}
