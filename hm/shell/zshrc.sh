#
# ZSH aliases and customizations
# Am not sure what everything does in there...
#

# TODO Learn `setopt extendedglob`

# powerline-go duration support
zmodload zsh/datetime
function preexec() {
    __TIMER=$EPOCHREALTIME
}

# Cursor based on mode
if [ $TERM = "linux" ]; then
    _LINE_CURSOR="\e[?0c"
    _BLOCK_CURSOR="\e[?8c"
else
    _LINE_CURSOR="\e[6 q"
    _BLOCK_CURSOR="\e[2 q"
fi
function zle-keymap-select zle-line-init
{
    case $KEYMAP in
        vicmd)      print -n -- "$_BLOCK_CURSOR";;
        viins|main) print -n -- "$_LINE_CURSOR";;
    esac
}
function zle-line-finish
{
    print -n -- "$_BLOCK_CURSOR"
}
zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

# Also return to normal mode from jk shortcut
bindkey 'jk' vi-cmd-mode

# Edit command line
autoload edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# Additional history-substring-search bindings for vi cmd mode
# TODO Doesn't work, as home-manager loads history-substring at the very end of the file
# bindkey -M vicmd 'k' history-substring-search-up
# bindkey -M vicmd 'j' history-substring-search-down

# Autocompletion
autoload -Uz promptinit
promptinit

# Color common prefix
zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==35=00}:${(s.:.)LS_COLORS}")'


setopt GLOBDOTS # Complete hidden files

setopt NO_BEEP # that annoying beep goes away
# setopt NO_LIST_BEEP # beeping is only turned off for ambiguous completions
#
setopt AUTO_LIST # when the completion is ambiguous you get a list without having to type ^D
# setopt BASH_AUTO_LIST # the list only happens the second time you hit tab on an ambiguous completion
# setopt LIST_AMBIGUOUS # this is modified so that nothing is listed if there is an unambiguous prefix or suffix to be inserted --- this can be combined with BASH_AUTO_LIST, so that where both are applicable you need to hit tab three times for a listing
unsetopt REC_EXACT # if the string on the command line exactly matches one of the possible completions, it is accepted, even if there is another completion (i.e. that string with something else added) that also matches
unsetopt MENU_COMPLETE # one completion is always inserted completely, then when you hit TAB it changes to the next, and so on until you get back to where you started
unsetopt AUTO_MENU # you only get the menu behaviour when you hit TAB again on the ambiguous completion.
unsetopt AUTO_REMOVE_SLASH

# Help
# TODO ~~Doesn't work (how ironic)~~ Works but it's just man?
autoload -Uz run-help
unalias run-help
alias help=run-help
autoload -Uz run-help-git
autoload -Uz run-help-ip
autoload -Uz run-help-openssl
autoload -Uz run-help-p4
autoload -Uz run-help-sudo
autoload -Uz run-help-svk
autoload -Uz run-help-svn

# Dir stack
DIRSTACKFILE="$HOME/.cache/zsh_dirstack"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
  dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
  # [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
  print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}

DIRSTACKSIZE=20

setopt AUTO_PUSHD PUSHD_SILENT PUSHD_TO_HOME
setopt PUSHD_IGNORE_DUPS # Remove duplicate entries
setopt PUSHD_MINUS # This reverts the +/- operators.


# History

# From https://unix.stackexchange.com/a/273863
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
unsetopt HIST_VERIFY               # Don't execute immediately upon history expansion.
unsetopt HIST_BEEP                 # Beep when accessing nonexistent history.

