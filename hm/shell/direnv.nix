# Allow switching to a specific environment when going into a directory
{
  ...
}:
{
  config = {
    programs = {
      direnv = {
        enable = true;
        enableBashIntegration = true;
        enableZshIntegration = true;
        nix-direnv.enable = true;
      };
      git.ignores = [ ".envrc" ".direnv" ];
    };
  };
}
