{
  pkgs,
  config,
  ...
}:
let
  cfg = config.programs.bash;
in
{
  config = {
    programs = {
      bash = {
        enable = true;
        bashrcExtra = ''
          shopt -s expand_aliases
          shopt -s histappend
        '';
        historySize = 100000;
        historyFile = "${config.xdg.stateHome}/shell_history";
        historyFileSize = 100000;
        historyControl = [
          "erasedups"
          "ignoredups"
          "ignorespace"
        ];
      };
      zsh = {
        enable = true;
        autosuggestion.enable = true;
        enableCompletion = true;
        syntaxHighlighting.enable = true;
        historySubstringSearch.enable = true;
        initExtra = builtins.readFile ./zshrc.sh;
        defaultKeymap = "viins";
        history = {
          size = cfg.historySize;
          save = cfg.historyFileSize;
          path = cfg.historyFile;
          expireDuplicatesFirst = true;
        };
        shellAliases = cfg.shellAliases;
        plugins = [
          {
            name = "zsh-nix-shell";
            file = "nix-shell.plugin.zsh";
            src = pkgs.fetchFromGitHub {
              owner = "chisui";
              repo = "zsh-nix-shell";
              rev = "v0.8.0";
              sha256 = "1lzrn0n4fxfcgg65v0qhnj7wnybybqzs4adz7xsrkgmcsr0ii8b7";
            };
          }
        ];
      };
    };
  };
  imports = [
    ./atuin.nix
    ./direnv.nix
  ];
}
