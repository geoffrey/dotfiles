{ pkgs, config, ... }:
{
  home.packages = [
    pkgs.update-local-flakes
    (pkgs.writeShellApplication {
      name = "rb";
      text = ''
        verb="switch"
        if [ "$#" -ge 1 ]
        then
          verb="$1"
          shift
        fi
        nixos_flake="$(readlink -f /etc/nixos)"
        if [ -f "$nixos_flake/flake.nix" ]
        then
          update-local-flakes "$nixos_flake"
          nix run "$nixos_flake#nixosRebuild" -- "$verb" "$@"
        fi
        # TODO Fix nix-on-droid and home-manager
        # hm_flake="${config.xdg.configHome}/home-manager/flake.nix"
        # if [ -f "$hm_flake" ]
        # then
        #   update-local-flakes "$hm_flake"
        #   home-manager "$verb" "$@"
        # fi
        # nod_flake="${config.xdg.configHome}/nix-on-droid/flake.nix"
        # if [ -f "$nod_flake" ]
        # then
        #   update-local-flakes "$nod_flake"
        #   nix-on-droid "$verb" --flake "$(dirname "$nod_flake")" "$@"
        # fi
      '';
    })
  ];
}
