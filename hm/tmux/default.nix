{
  pkgs,
  lib,
  config,
  ...
}:
let
  themepack = pkgs.tmuxPlugins.mkTmuxPlugin rec {
    pluginName = "tmux-themepack";
    version = "1.1.0";
    rtpFilePath = "themepack.tmux";
    src = pkgs.fetchFromGitHub {
      owner = "jimeh";
      repo = "tmux-themepack";
      rev = "${version}";
      sha256 = "f6y92kYsKDFanNx5ATx4BkaB/E7UrmyIHU/5Z01otQE=";
    };
  };
in
{
  config = lib.mkIf config.programs.tmux.enable {
    home = {
      packages = [ pkgs.screen ];
      sessionVariables.SCREENRC = "${config.xdg.configHome}/screenrc";
    };
    programs.tmux = {
      mouse = false;
      clock24 = true;
      # TODO Vim mode?
      plugins = with pkgs.tmuxPlugins; [
        sensible
      ];
      extraConfig =
        builtins.readFile ./tmux.conf
        + "source-file ${themepack}/share/tmux-plugins/tmux-themepack/powerline/default/green.tmuxtheme\n";
    };
    stylix.targets.tmux.enable = false;
    xdg.configFile.screenrc.text = (builtins.readFile ./screenrc);
  };
}
