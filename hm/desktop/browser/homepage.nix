{
  pkgs,
  lib,
  config,
  ...
}:
let
  # TODO ForkAwesome is deprecated, find something else
  fa = pkgs.fetchFromGitHub {
    owner = "ForkAwesome";
    repo = "Fork-Awesome";
    rev = "1.2.0";
    sha256 = "sha256-zG6/0dWjU7/y/oDZuSEv+54Mchng64LVyV8bluskYzc=";
  };
  data = config.frogeye.homepage // {
    sections = builtins.attrValues config.frogeye.homepage.sections;
    css = ./homepage.css;
    fa_css = "${fa}/css/fork-awesome.min.css";
  };
  # Blatantly stolen from https://pablo.tools/blog/computers/nix-mustache-templates/
  homepage = builtins.toString (
    pkgs.stdenv.mkDerivation {

      name = "homepage.html";

      nativeBuildInpts = [ pkgs.mustache-go ];

      passAsFile = [ "jsonData" ];
      jsonData = builtins.toJSON data;

      phases = [
        "buildPhase"
        "installPhase"
      ];

      buildPhase = ''
        ${pkgs.mustache-go}/bin/mustache $jsonDataPath ${./homepage.html.mustache} > homepage.html
      '';

      installPhase = ''
        cp homepage.html $out
      '';
    }
  );
in
{
  config.programs = {
    firefox.profiles.hm.settings."browser.startup.homepage" = homepage;
    qutebrowser.settings.url = {
      start_pages = homepage;
      default_page = homepage;
    };
  };
  options.frogeye.homepage = {
    sections = lib.mkOption {
      default = { };
      description = "Folders used by users";
      # Top-level so Syncthing can work for all users. Also there's no real home-manager syncthing module.
      type = lib.types.attrsOf (
        lib.types.submodule (
          { config, name, ... }:
          {
            options = {
              title = lib.mkOption {
                type = lib.types.str;
                default = "Section title";
              };
              color = lib.mkOption {
                type = lib.types.str;
                default = "#337ab7";
              };
              image = lib.mkOption {
                type = lib.types.nullOr lib.types.path;
                default = null;
              };
              url = lib.mkOption {
                type = lib.types.str;
                default = "about:blank";
              };
              links = lib.mkOption {
                default = [ ];
                type = lib.types.listOf (
                  lib.types.submodule (
                    { config, ... }:
                    {
                      options = {
                        name = lib.mkOption {
                          type = lib.types.str;
                          default = "Link";
                        };
                        url = lib.mkOption {
                          type = lib.types.str;
                          default = "about:blank";
                        };
                        icon = lib.mkOption {
                          type = lib.types.str;
                          default = "question-circle";
                        };
                      };
                    }
                  )
                );
              };
            };
          }
        )
      );
    };
  };
}
