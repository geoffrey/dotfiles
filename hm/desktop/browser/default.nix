{
  pkgs,
  lib,
  config,
  ...
}:
{
  imports = [
    ./homepage.nix
  ];
  config = lib.mkIf config.frogeye.desktop.xorg {
    home.sessionVariables = {
      BROWSER = "qutebrowser";
    };
    programs = {
      firefox = {
        enable = true;
        package = pkgs.firefox.override {
          nativeMessagingHosts = [
            pkgs.tridactyl-native
          ];
        };

        profiles.hm = {
          extensions = with pkgs.nur.repos.rycee.firefox-addons; [
            (buildFirefoxXpiAddon {
              pname = "onetab";
              version = "0.1.0";
              addonId = "onetab@nated";
              url = "https://addons.mozilla.org/firefox/downloads/file/4118712/one_tab_per_window-0.1.0.xpi";
              sha256 = "sha256-64DeL2xgXpqz32LJWDx4jhS2Fvbld8re3z8fdwnNTw0=";
              meta = with lib; {
                homepage = "https://git.sr.ht/~nated/onetab";
                description = "When a new tab is opened, redirects it to a new window instead.";
                license = licenses.unfree;
                mozPermissions = [ "tabs" ];
                platforms = platforms.all;
              };
            })
            tridactyl
            ublock-origin
          ];
          search = {
            default = "DuckDuckGo";
            engines = {
              # TODO Harmonize with qutebrowser search engines
              "Nix Packages" = {
                urls = [
                  {
                    template = "https://search.nixos.org/packages";
                    params = [
                      {
                        name = "type";
                        value = "packages";
                      }
                      {
                        name = "query";
                        value = "{searchTerms}";
                      }
                    ];
                  }
                ];
                icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
                definedAliases = [ "@np" ];
              };
              "NixOS Wiki" = {
                urls = [ { template = "https://nixos.wiki/index.php?search={searchTerms}"; } ];
                iconUpdateURL = "https://nixos.wiki/favicon.png";
                updateInterval = 24 * 60 * 60 * 1000; # every day
                definedAliases = [ "@nw" ];
              };
              "Bing".metaData.hidden = true;
              "Google".metaData.alias = "@g"; # builtin engines only support specifying one additional alias
            };
            force = true;
          };
          settings = {
            "signon.rememberSignons" = false; # Don't save passwords
            "browser.newtabpage.enabled" = false; # Best would be homepage but not possible without extension?
            # Europe please
            "browser.search.region" = "GB";
            "browser.search.isUS" = false;
            "distribution.searchplugins.defaultLocale" = "en-GB";
            "general.useragent.locale" = "en-GB";
          };
        };
      };
      qutebrowser = {
        enable = true;
        keyBindings = {
          normal = {
            # Match tab behaviour to i3. Not that I use tabs.
            "H" = "tab-prev";
            "J" = "back";
            "K" = "forward";
            "L" = "tab-next";
            # "T" = null;
            "af" = "spawn --userscript freshrss"; # TODO Broken?
            "as" = "spawn --userscript shaarli"; # TODO I don't use shaarli anymore
            # "d" = null;
            "u" = "undo --window";
            # TODO Unbind d and T (?)
          };
        };
        loadAutoconfig = true;
        searchEngines = rec {
          DEFAULT = ecosia;
          alpinep = "https://pkgs.alpinelinux.org/packages?name={}&branch=edge";
          ampwhat = "http://www.amp-what.com/unicode/search/{}";
          arch = "https://wiki.archlinux.org/?search={}";
          archp = "https://www.archlinux.org/packages/?q={}";
          aur = "https://aur.archlinux.org/packages/?K={}";
          aw = ampwhat;
          ddg = duckduckgo;
          dockerhub = "https://hub.docker.com/search/?isAutomated=0&isOfficial=0&page=1&pullCount=0&q={}&starCount=0";
          duckduckgo = "https://duckduckgo.com/?q={}&ia=web";
          ecosia = "https://www.ecosia.org/search?q={}";
          gfr = "https://www.google.fr/search?hl=fr&q={}";
          g = google;
          gh = github;
          gi = "http://images.google.com/search?q={}";
          giphy = "https://giphy.com/search/{}";
          github = "https://github.com/search?q={}";
          google = "https://www.google.fr/search?q={}";
          hm = homemanager;
          homemanager = "https://home-manager-options.extranix.com/?query={}&release=${config.home.version.release}";
          invidious = "https://invidious.frogeye.fr/search?q={}";
          inv = invidious;
          nixos = "https://search.nixos.org/options?channel=${config.home.version.release}&query={}";
          nixoswiki = "https://wiki.nixos.org/w/index.php?search={}";
          nixpkgs = "https://search.nixos.org/packages?channel=${config.home.version.release}&query={}";
          noogle = "https://noogle.dev/q?term={}";
          npm = "https://www.npmjs.com/search?q={}";
          nw = nixoswiki;
          os = nixos;
          pkgs = nixpkgs;
          q = qwant;
          qwant = "https://www.qwant.com/?t=web&q={}";
          wolfram = "https://www.wolframalpha.com/input/?i={}";
          youtube = "https://www.youtube.com/results?search_query={}";
          yt = youtube;
        };
        settings = {
          colors.webpage.darkmode.policy.images = "never"; # No inverting images in dark mode, is ugly
          downloads.location.prompt = false;
          tabs = {
            show = "never";
            tabs_are_windows = true;
          };
          url.open_base_url = true;
          content = {
            # I had this setting below, not sure if it did something special
            # config.set("content.cookies.accept", "no-3rdparty", "chrome://*/*")
            cookies.accept = "no-3rdparty";
            prefers_reduced_motion = true;
            headers.accept_language = "en-GB,en;q=0.9";
            tls.certificate_errors = "ask-block-thirdparty";
            javascript.clipboard = "access"; # copy-paste is fine
          };
          editor.command = [
            "${pkgs.neovide}/bin/neovide"
            "--"
            "-f"
            "{file}"
            "-c"
            "normal {line}G{column0}l"
          ];
          # TODO Doesn't work on Arch. Does it even load the right profile on Nix?
          # TODO spellcheck.languages = ["fr-FR" "en-GB" "en-US"];
        };
      };
    };
    xdg = {
      configFile."tridactyl/tridactylrc".source = ./tridactylrc; # TODO Improve that :)
      mimeApps = {
        enable = true;
        defaultApplications = {
          "text/html" = "org.qutebrowser.qutebrowser.desktop";
          "x-scheme-handler/http" = "org.qutebrowser.qutebrowser.desktop";
          "x-scheme-handler/https" = "org.qutebrowser.qutebrowser.desktop";
          "x-scheme-handler/about" = "org.qutebrowser.qutebrowser.desktop";
          "x-scheme-handler/unknown" = "org.qutebrowser.qutebrowser.desktop";
        };
      };
    };
    xsession.windowManager.i3.config.keybindings = {
      "${config.xsession.windowManager.i3.config.modifier}+m" =
        "exec ${config.programs.qutebrowser.package}/bin/qutebrowser --override-restore";
    };
  };
}
