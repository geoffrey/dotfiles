{
  pkgs,
  lib,
  config,
  ...
}:
let
  pactl = "exec ${pkgs.pulseaudio}/bin/pactl"; # TODO Use NixOS package if using NixOS
  mod = config.xsession.windowManager.i3.config.modifier;
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    home = {
      packages = with pkgs; [
        pwvucontrol # Because can't use Win+F1X on Pinebook 🙃
        pavucontrol # Just in case
        helvum
        qpwgraph
        sox
      ];
      sessionVariables = {
        ALSA_PLUGIN_DIR = "${pkgs.alsa-plugins}/lib/alsa-lib"; # Fixes an issue with sox (Cannot open shared library libasound_module_pcm_pulse.so)
        # UPST Patch this upstream like: https://github.com/NixOS/nixpkgs/blob/216b111fb87091632d077898df647d1438fc2edb/pkgs/applications/audio/espeak-ng/default.nix#L84
      };
    };
    programs.bash.shellAliases = {
      beep = ''${pkgs.sox}/bin/play -n synth sine E5 sine A4 remix 1-2 fade 0.5 1.2 0.5 2> /dev/null'';
      noise = ''${pkgs.sox}/bin/play -c 2 -n synth $'' + ''{1}noise'';
    };
    xdg.configFile = {
      "pulse/client.conf" = {
        text = ''cookie-file = .config/pulse/pulse-cookie'';
      };
    };
    xsession.windowManager.i3.config.keybindings = {
      "XF86AudioRaiseVolume" = "${pactl} set-sink-mute @DEFAULT_SINK@ false; ${pactl} set-sink-volume @DEFAULT_SINK@ +5%";
      "XF86AudioLowerVolume" = "${pactl} set-sink-mute @DEFAULT_SINK@ false; ${pactl} set-sink-volume @DEFAULT_SINK@ -5%";
      "XF86AudioMute" = "${pactl} set-sink-mute @DEFAULT_SINK@ true";
      "${mod}+F8" = "${pactl} suspend-sink @DEFAULT_SINK@ 1; ${pactl} suspend-sink @DEFAULT_SINK@ 0"; # Re-synchronize bluetooth headset
      "${mod}+F11" = "exec ${pkgs.pavucontrol}/bin/pwvucontrol";
      # TODO Find pacmixer?
    };
  };
}
