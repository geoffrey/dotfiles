{
  pkgs,
  lib,
  config,
  ...
}:
let
  # FOCUS
  focus = "exec ${pkgs.writeShellScript "i3-focus-window" ''
    WINDOW=`${pkgs.xdotool}/bin/xdotool getwindowfocus`
    eval `${pkgs.xdotool}/bin/xdotool getwindowgeometry --shell $WINDOW` # this brings in variables WIDTH and HEIGHT
    TX=`${pkgs.coreutils}/bin/expr $WIDTH / 2`
    TY=`${pkgs.coreutils}/bin/expr $HEIGHT / 2`
    ${pkgs.xdotool}/bin/xdotool mousemove -window $WINDOW $TX $TY
  ''}";

  # CARDINALS
  cardinals = [
    {
      vi = "h";
      arrow = "Left";
      container = "left";
      workspace = "prev_on_output";
      output = "left";
    }
    {
      vi = "l";
      arrow = "Right";
      container = "right";
      workspace = "next_on_output";
      output = "right";
    }
    {
      vi = "j";
      arrow = "Down";
      container = "down";
      workspace = "prev";
      output = "below";
    }
    {
      vi = "k";
      arrow = "Up";
      container = "up";
      workspace = "next";
      output = "above";
    }
  ];
  forEachCardinal = f: map (c: f c) cardinals;

  # WORKSPACES
  workspaces_keys = lib.strings.stringToCharacters "1234567890";
  workspaces = map (i: {
    id = i;
    name = builtins.toString (i + 1);
    key = builtins.elemAt workspaces_keys i;
  }) (lib.lists.range 0 ((builtins.length workspaces_keys) - 1));
  forEachWorkspace = f: map (w: f w) workspaces;

  # MISC
  mod = config.xsession.windowManager.i3.config.modifier;
  rofi = "exec --no-startup-id ${config.programs.rofi.package}/bin/rofi";
  modes = config.frogeye.desktop.i3.bindmodes;
  x11_screens = config.frogeye.desktop.x11_screens;
in
{
  config = lib.mkIf config.xsession.windowManager.i3.enable {
    stylix.targets.i3.enable = false;
    services.picom.enable = true;
    xdg.configFile = {
      "rofimoji.rc" = {
        text = ''
          skin-tone = neutral
          files = [emojis, math]
          action = clipboard
        '';
      };
    };
    xsession.windowManager.i3.config = {
      modifier = lib.mkDefault "Mod4";
      fonts = {
        names = [ config.stylix.fonts.sansSerif.name ];
      };
      terminal = "alacritty";
      colors =
        let
          ignore = "#ff00ff";
        in
        with config.lib.stylix.colors.withHashtag;
        lib.mkForce {
          focused = {
            border = base0B;
            background = base0B;
            text = base00;
            indicator = base00;
            childBorder = base0B;
          };
          focusedInactive = {
            border = base02;
            background = base02;
            text = base05;
            indicator = base02;
            childBorder = base02;
          };
          unfocused = {
            border = base05;
            background = base04;
            text = base00;
            indicator = base04;
            childBorder = base00;
          };
          urgent = {
            border = base0F;
            background = base08;
            text = base00;
            indicator = base08;
            childBorder = base0F;
          };
          placeholder = {
            border = ignore;
            background = base00;
            text = base05;
            indicator = ignore;
            childBorder = base00;
          };
          background = base07;
          # I set the color of the active tab as the the background color of the terminal so they merge together.
        };
      focus.followMouse = false;
      keybindings =
        {
          # Compatibility layer for people coming from other backgrounds
          "Mod1+Tab" = "${rofi} -modi window -show window";
          "Mod1+F2" = "${rofi} -modi drun -show drun";
          "Mod1+F4" = "kill";
          # kill focused window
          "${mod}+z" = "kill";
          button2 = "kill";
          # Rofi
          "${mod}+i" = "exec --no-startup-id ${pkgs.rofimoji}/bin/rofimoji";
          # start program launcher
          "${mod}+d" = "${rofi} -modi run -show run";
          "${mod}+Shift+d" = "${rofi} -modi drun -show drun";
          # Start Applications
          "${mod}+p" = "exec ${pkgs.xfce.thunar}/bin/thunar";
          # Misc
          "${mod}+F10" = "exec ${pkgs.writeShellScript "show-keyboard-layout" ''
            layout=`${pkgs.xorg.setxkbmap}/bin/setxkbmap -query | ${pkgs.gnugrep}/bin/grep ^layout: | ${pkgs.gawk}/bin/awk '{ print $2 }'`
            ${pkgs.libgnomekbd}/bin/gkbd-keyboard-display -l $layout
          ''}";
          # workspace back and forth (with/without active container)
          "${mod}+b" = "workspace back_and_forth; ${focus}";
          "${mod}+Shift+b" = "move container to workspace back_and_forth; workspace back_and_forth; ${focus}";
          # Change container layout
          "${mod}+g" = "split h; ${focus}";
          "${mod}+v" = "split v; ${focus}";
          "${mod}+f" = "fullscreen toggle; ${focus}";
          "${mod}+s" = "layout stacking; ${focus}";
          "${mod}+w" = "layout tabbed; ${focus}";
          "${mod}+e" = "layout toggle split; ${focus}";
          "${mod}+Shift+space" = "floating toggle; ${focus}";
          # Focus container
          "${mod}+space" = "focus mode_toggle; ${focus}";
          "${mod}+a" = "focus parent; ${focus}";
          "${mod}+q" = "focus child; ${focus}";
          # i3 control
          "${mod}+Shift+c" = "reload";
          "${mod}+Shift+r" = "restart";
          "${mod}+Shift+e" = "exit";
        }
        // lib.mapAttrs' (k: v: lib.nameValuePair v.enter "mode ${v.name}") (
          lib.filterAttrs (k: v: v.enter != null) modes
        )
        // lib.attrsets.mergeAttrsList (
          forEachCardinal (c: {
            # change focus
            "${mod}+${c.vi}" = "focus ${c.container}; ${focus}";
            # move focused window
            "${mod}+Shift+${c.vi}" = "move ${c.container}; ${focus}";
            #navigate workspaces next / previous
            "${mod}+Ctrl+${c.vi}" = "workspace ${c.workspace}; ${focus}";
            # Move to workspace next / previous with focused container
            "${mod}+Ctrl+Shift+${c.vi}" = "move container to workspace ${c.workspace}; workspace ${c.workspace}; ${focus}";
            # move workspaces to screen (arrow keys)
            "${mod}+Ctrl+Shift+${c.arrow}" = "move workspace to output ${c.output}; ${focus}";
          })
        )
        // lib.attrsets.mergeAttrsList (
          forEachWorkspace (w: {
            # Switch to workspace
            "${mod}+${w.key}" = "workspace ${w.name}; ${focus}";
            # move focused container to workspace
            "${mod}+ctrl+${w.key}" = "move container to workspace ${w.name}; ${focus}";
            # move to workspace with focused container
            "${mod}+shift+${w.key}" = "move container to workspace ${w.name}; workspace ${w.name}; ${focus}";
          })
        );
      modes = lib.mapAttrs' (
        k: v:
        lib.nameValuePair v.name (
          v.bindings
          // lib.optionalAttrs v.return_bindings {
            "Return" = "mode default";
            "Escape" = "mode default";
          }
        )
      ) modes;
      window = {
        hideEdgeBorders = "both";
        titlebar = false; # So that single-container screens are basically almost fullscreen
        commands = [
          # switch to workspace with urgent window automatically
          {
            criteria = {
              urgent = "latest";
            };
            command = "focus";
          }
        ];
      };
      floating = {
        criteria = [
          { window_role = "pop-up"; }
          { window_role = "task_dialog"; }
        ];
      };
      startup = [
        {
          notification = false;
          command = "${
            pkgs.writeShellApplication {
              name = "batteryNotify";
              runtimeInputs = with pkgs; [
                coreutils
                libnotify
              ];
              text = builtins.readFile ./batteryNotify.sh;
              # TODO Use batsignal instead?
              # TODO Only on computers with battery
            }
          }/bin/batteryNotify";
        }
      ];
      workspaceLayout = "tabbed";
      focus.mouseWarping = true; # i3 only supports warping to workspace, hence ${focus}
      workspaceOutputAssign = forEachWorkspace (w: {
        output = builtins.elemAt x11_screens (lib.mod w.id (builtins.length x11_screens));
        workspace = w.name;
      });
    };
    frogeye.desktop.i3.bindmodes = {
      "Resize" = {
        bindings = {
          "h" = "resize shrink width 10 px or 10 ppt; ${focus}";
          "j" = "resize grow height 10 px or 10 ppt; ${focus}";
          "k" = "resize shrink height 10 px or 10 ppt; ${focus}";
          "l" = "resize grow width 10 px or 10 ppt; ${focus}";
        };
        mod_enter = "r";
      };
      "[L] Vérouillage [E] Déconnexion [S] Veille [H] Hibernation [R] Redémarrage [P] Extinction" = {
        bindings = {
          "l" = "exec --no-startup-id exec xlock, mode default";
          "e" = "exit, mode default";
          "s" = "exec --no-startup-id exec xlock & ${pkgs.systemd}/bin/systemctl suspend --check-inhibitors=no, mode default";
          "h" = "exec --no-startup-id exec xlock & ${pkgs.systemd}/bin/systemctl hibernate, mode default";
          "r" = "exec --no-startup-id ${pkgs.systemd}/bin/systemctl reboot, mode default";
          "p" = "exec --no-startup-id ${pkgs.systemd}/bin/systemctl poweroff -i, mode default";
        };
        mod_enter = "Escape";
      };
    };
  };
  options = {
    frogeye.desktop.i3.bindmodes = lib.mkOption {
      default = { };
      type = lib.types.attrsOf (
        lib.types.submodule (
          { config, name, ... }:
          {
            options = {
              name = lib.mkOption {
                type = lib.types.str;
                default = name;
              };
              bindings = lib.mkOption {
                type = lib.types.attrsOf lib.types.str;
                default = { };
              };
              enter = lib.mkOption {
                type = lib.types.nullOr lib.types.str;
                default = "${mod}+${config.mod_enter}";
              };
              mod_enter = lib.mkOption {
                type = lib.types.str;
              };
              return_bindings = lib.mkOption {
                type = lib.types.bool;
                default = true;
              };
            };
          }
        )
      );
    };
  };
}
