{
  pkgs,
  config,
  ...
}:
{
  config = {
    # This correctly sets the background on some occasions, below does the rest
    programs.autorandr.hooks.postswitch = {
      background = "${pkgs.feh}/bin/feh --no-fehbg --bg-fill ${config.stylix.image}";
    };
  };
}
