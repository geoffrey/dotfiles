{
  pkgs,
  lib,
  config,
  ...
}:
let
  mod = config.xsession.windowManager.i3.config.modifier;
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    home.sessionVariables = {
      RXVT_SOCKET = "${config.xdg.stateHome}/urxvtd";
      # We don't use urxvt deamon mode as we use it as a backup, but just in case, this helps keep it out of the home directory.
    };
    programs = {
      alacritty = {
        # TODO Emojis
        # Arch (working) shows this with alacritty -vvv:
        # [TRACE] [crossfont] Got font path="/usr/share/fonts/twemoji/twemoji.ttf", index=0
        # [DEBUG] [crossfont] Loaded Face Face { ft_face: Font Face: Regular, load_flags: MONOCHROME | TARGET_MONO | COLOR, render_mode: "Mono", lcd_filter: 1 }
        # Nix (not working) shows this:
        # [TRACE] [crossfont] Got font path="/nix/store/872g3w9vcr5nh93r0m83a3yzmpvd2qrj-home-manager-path/share/fonts/truetype/TwitterColorEmoji-SVGinOT.ttf", index=0
        # [DEBUG] [crossfont] Loaded Face Face { ft_face: Font Face: Regular, load_flags: TARGET_LIGHT | COLOR, render_mode: "Lcd", lcd_filter: 1 }

        enable = true;
        settings = {
          bell = {
            animation = "EaseOutExpo";
            color = "#000000";
            command = {
              program = "${pkgs.sox}/bin/play";
              args = [
                "-n"
                "synth"
                "sine"
                "C5"
                "sine"
                "E4"
                "remix"
                "1-2"
                "fade"
                "0.1"
                "0.2"
                "0.1"
              ];
            };
            duration = 100;
          };
          cursor = {
            vi_mode_style = "Underline";
          };
          env = {
            WINIT_X11_SCALE_FACTOR = "1";
            # Prevents Alacritty from resizing from one monitor to another.
            # Might cause issue on HiDPI screens but we'll get there when we get there
          };
          hints = {
            enabled = [
              {
                binding = {
                  mods = "Control|Alt";
                  key = "F";
                };
                command = "${pkgs.xdg-utils}/bin/xdg-open";
                mouse = {
                  enabled = true;
                  mods = "Control";
                };
                post_processing = true;
                regex = "(mailto:|gemini:|gopher:|https:|http:|news:|file:|git:|ssh:|ftp:)[^\\u0000-\\u001F\\u007F-\\u009F<>\"\\\\s{-}\\\\^⟨⟩`]+";
              }
            ];
          };
          keyboard.bindings = [
            {
              mode = "~Search";
              mods = "Alt|Control";
              key = "Space";
              action = "ToggleViMode";
            }
            {
              mode = "Vi|~Search";
              mods = "Control";
              key = "K";
              action = "ScrollHalfPageUp";
            }
            {
              mode = "Vi|~Search";
              mods = "Control";
              key = "J";
              action = "ScrollHalfPageDown";
            }
            {
              mode = "~Vi";
              mods = "Control|Alt";
              key = "V";
              action = "Paste";
            }
            {
              mods = "Control|Alt";
              key = "C";
              action = "Copy";
            }
            {
              mode = "~Search";
              mods = "Control|Alt";
              key = "F";
              action = "SearchForward";
            }
            {
              mode = "~Search";
              mods = "Control|Alt";
              key = "B";
              action = "SearchBackward";
            }
            {
              mode = "Vi|~Search";
              mods = "Control|Alt";
              key = "C";
              action = "ClearSelection";
            }
          ];
          window = {
            dynamic_padding = false;
            dynamic_title = true;
          };
        };
      };
      # Backup terminal
      urxvt = {
        enable = true;
        package = pkgs.rxvt-unicode-emoji;
        scroll = {
          bar.enable = false;
        };
        iso14755 = false; # Disable Ctrl+Shift default bindings
        keybindings = {
          "Shift-Control-C" = "eval:selection_to_clipboard";
          "Shift-Control-V" = "eval:paste_clipboard";
          # TODO Not sure resizing works, Nix doesn't have the package (urxvt-resize-font-git on Arch)
          "Control-KP_Subtract" = "resize-font:smaller";
          "Control-KP_Add" = "resize-font:bigger";
        };
        extraConfig = {
          "letterSpace" = 0;
          "perl-ext-common" = "resize-font,bell-command,readline,selection";
          "bell-command" = "${pkgs.sox}/bin/play -n synth sine C5 sine E4 remix 1-2 fade 0.1 0.2 0.1 &> /dev/null";
        };
      };
    };
    xsession.windowManager.i3.config.keybindings = {
      "${mod}+Return" = "exec ${config.programs.alacritty.package}/bin/alacritty msg create-window -e zsh || exec ${config.programs.alacritty.package}/bin/alacritty -e zsh";
      # -e zsh is for systems where I can't configure my user's shell
      "${mod}+Shift+Return" = "exec ${config.programs.urxvt.package}/bin/urxvt";
    };
  };
}
