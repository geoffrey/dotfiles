{
  pkgs,
  lib,
  config,
  ...
}:
let
  # UPST
  sct = pkgs.sct.overrideAttrs (old: {
    patches = (old.patches or [ ]) ++ [
      ./sct_aarch64.patch
    ];
  });
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    frogeye.desktop.i3.bindmodes = {
      "Temperature [R] Red [D] Dust storm [C] Campfire [O] Normal [A] All nighter [B] Blue" = {
        bindings = {
          "r" = "exec ${sct}/bin/sct 1000";
          "d" = "exec ${sct}/bin/sct 2000";
          "c" = "exec ${sct}/bin/sct 4500";
          "o" = "exec ${sct}/bin/sct";
          "a" = "exec ${sct}/bin/sct 8000";
          "b" = "exec ${sct}/bin/sct 10000";
        };
        mod_enter = "y";
      };
    };
    home.packages = [ sct ];
  };
}
