# Dual-screen presenting for slideshows and stuff.
# Not tested since Nix.
# Config mentions pdfpc, although the last thing I used was Impressive, even made patches to it.
# UPST Add Impressive to nixpkgs
{
  lib,
  config,
  ...
}:
let
  mode_pres_main = "Presentation (main display)";
  mode_pres_sec = "Presentation (secondary display)";
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    frogeye.desktop.i3.bindmodes = {
      "${mode_pres_main}" = {
        mod_enter = "Shift+p";
        bindings = {
          "b" = "workspace 3, workspace 4, mode ${mode_pres_sec}";
          "q" = "mode default";
          "Return" = "mode default";
        };
        return_bindings = false;
      };
      "${mode_pres_sec}" = {
        enter = null;
        bindings = {
          "b" = "workspace 1, workspace 2, mode ${mode_pres_main}";
          "q" = "mode default";
          "Return" = "mode default";
        };
        return_bindings = false;
      };
    };
    xsession.windowManager.i3.config.window.commands = [
      # Open specific applications in floating mode
      {
        criteria = {
          title = "^pdfpc.*";
          window_role = "presenter";
        };
        command = "move to output left, fullscreen";
      }
      {
        criteria = {
          title = "^pdfpc.*";
          window_role = "presentation";
        };
        command = "move to output right, fullscreen";
      }
    ];
  };

}
