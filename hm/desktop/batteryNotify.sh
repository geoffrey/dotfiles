BATT="/sys/class/power_supply/BAT0"
LOW=10
CRIT=3
LASTSTATE="$HOME/.cache/batteryState"

function setState() { # state [...notify-send arguments]
    state="$1"
    last="$(cat "$LASTSTATE" 2> /dev/null)"
    shift

    echo "Battery state: $state"

    if [ "$state" != "$last" ]
    then
        notify-send "$@"
        echo "$state" > "$LASTSTATE"
    fi
}

function computeState() {
    acpiStatus="$(cat "$BATT/status")"
    acpiCapacity="$(cat "$BATT/capacity")"

    if [ "$acpiStatus" == "Discharging" ]
    then
        if [ "$acpiCapacity" -le $CRIT ]
        then
            setState "CRIT" -u critical -i battery-caution "Battery level is critical" "$acpiCapacity %"
        elif [ "$acpiCapacity" -le $LOW ]
        then
            setState "LOW" -u critical -i battery-low "Battery level is low" "$acpiCapacity %"
        else
            setState "DISCHARGING" -i battery-good "Battery is discharging" "$acpiCapacity %"
        fi
    elif [ "$acpiStatus" == "Charging" ]
    then
        setState "CHARGING" -u normal -i battery-good-charging "Battery is charging" "$acpiCapacity %"
    elif [ "$acpiStatus" == "Full" ]
    then
        setState "FULL" -u low -i battery-full-charged "Battery is full" "$acpiCapacity %"
    fi
}

if [ "$1" == "-d" ]
then
    while true
    do
        computeState
        sleep 10
    done
else
    computeState
fi
