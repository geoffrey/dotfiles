{
  pkgs,
  lib,
  config,
  ...
}:
let
  dir = config.xdg.userDirs.extraConfig.XDG_SCREENSHOTS_DIR;
  scrot = "${pkgs.scrot}/bin/scrot --exec '${pkgs.coreutils}/bin/mv $f ${dir}/ && ${pkgs.optipng}/bin/optipng ${dir}/$f'";
  mod = config.xsession.windowManager.i3.config.modifier;
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    frogeye.folders.screenshots.path = "Screenshots";
    xsession.windowManager.i3.config.keybindings = {
      "Print" = "exec ${scrot} --focused";
      "${mod}+Print" = "exec ${scrot}";
      "Ctrl+Print" = "--release exec ${scrot} --select";
    };
  };
}
