{
  pkgs,
  lib,
  config,
  ...
}:
let
  builtin_configs = [
    "off"
    "common"
    "clone-largest"
    "horizontal"
    "vertical"
    "horizontal-reverse"
    "vertical-reverse"
  ];
  autorandrmenu =
    {
      title,
      option,
      builtin ? false,
    }:
    pkgs.writeShellScript "autorandrmenu" ''
      shopt -s nullglob globstar
      profiles="${
        if builtin then lib.strings.concatLines builtin_configs else ""
      }$(${pkgs.autorandr}/bin/autorandr | ${pkgs.gawk}/bin/awk '{ print $1 }')"
      profile="$(echo "$profiles" | ${config.programs.rofi.package}/bin/rofi -dmenu -p "${title}")"
      [[ -n "$profile" ]] || exit
      ${pkgs.autorandr}/bin/autorandr ${option} "$profile"
    '';
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    frogeye.desktop.i3.bindmodes = {
      "Screen setup [A] Auto [L] Load [S] Save [R] Remove [D] Default" = {
        bindings = {
          "a" = "exec ${pkgs.autorandr}/bin/autorandr --change --force, mode default";
          "l" = "exec ${
            autorandrmenu {
              title = "Load profile";
              option = "--load";
              builtin = true;
            }
          }, mode default";
          "s" = "exec ${
            autorandrmenu {
              title = "Save profile";
              option = "--save";
            }
          }, mode default";
          "r" = "exec ${
            autorandrmenu {
              title = "Remove profile";
              option = "--remove";
            }
          }, mode default";
          "d" = "exec ${
            autorandrmenu {
              title = "Default profile";
              option = "--default";
              builtin = true;
            }
          }, mode default";
        };
        mod_enter = "t";
      };
    };
    programs.autorandr.enable = true;
    services.autorandr.enable = true;
  };
}
