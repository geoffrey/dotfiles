{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    home = {
      packages = with pkgs; [
        ashuffle
        mpc-cli
        vimpc
        playerctl
      ];
      sessionVariables = {
        MPD_PORT = "${toString config.services.mpd.network.port}";
      };
    };
    services = {
      mpd = {
        enable = true;
        network = {
          listenAddress = "0.0.0.0"; # Can be controlled remotely, determined with firewall
          startWhenNeeded = true;
        };
        extraConfig = ''
          restore_paused "yes"
          audio_output {
            type "pipewire"
            name "PipeWire Sound Server"
          }
        '';
        # UPST auto audio_output ?
        musicDirectory = "${config.home.homeDirectory}/Musiques";
      };
      # Expose mpd to mpris
      # mpd-mpris also exists but is MIT and make playerctld not pick up on play/pause events
      mpdris2.enable = true;
      # Allow control from headset
      mpris-proxy.enable = true;
      # Remember the last player
      playerctld.enable = true;
    };
    xdg = {
      configFile = {
        "vimpc/vimpcrc" = {
          text = ''
            map FF :browse<C-M>gg/
            map à :set add next<C-M>a:set add end<C-M>
            map @ :set add next<C-M>a:set add end<C-M>:next<C-M>
            map ° D:browse<C-M>A:shuffle<C-M>:play<C-M>:playlist<C-M>
            set songformat {%a - %b: %t}|{%f}$E$R $H[$H%l$H]$H
            set libraryformat %n \| {%t}|{%f}$E$R $H[$H%l$H]$H
            set ignorecase
            set sort library
          '';
        };
      };
    };
    xsession.windowManager.i3.config.keybindings = {
      "XF86AudioPrev" = "exec ${lib.getExe pkgs.playerctl} previous";
      "XF86AudioPlay" = "exec ${lib.getExe pkgs.playerctl} play-pause";
      "XF86AudioNext" = "exec ${lib.getExe pkgs.playerctl} next";
    };
  };
}
