{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    xsession.windowManager.i3.config.bars = [ ];
    programs.autorandr.hooks.postswitch = {
      frobar = "${pkgs.systemd}/bin/systemctl --user restart frobar";
    };
    systemd.user.services.frobar = {
      Unit = {
        Description = "frobar";
        After = [ "graphical-session-pre.target" ];
        PartOf = [ "graphical-session.target" ];
      };

      Service = {
        # Wait for i3 to start. Can't use ExecStartPre because otherwise it blocks graphical-session.target, and there's nothing i3/systemd
        # TODO Do that better
        ExecStart = ''${pkgs.bash}/bin/bash -c "while ! ${pkgs.i3}/bin/i3-msg; do ${pkgs.coreutils}/bin/sleep 1; done; ${pkgs.callPackage ./. { }}/bin/frobar"'';
      };

      Install = {
        WantedBy = [ "graphical-session.target" ];
      };
    };
  };
}
# TODO Connection with i3 is lost on start sometimes, more often than with Arch?
