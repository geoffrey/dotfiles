from setuptools import setup

setup(
    name="frobar",
    version="3.0",
    install_requires=[
        "i3ipc",
        "psutil",
        "pulsectl-asyncio",
        "pygobject3",
        "rich",
    ],
    entry_points={
        "console_scripts": [
            "frobar = frobar:main",
        ]
    },
)
