{
  pkgs ? import <nixpkgs> {
    config = { };
    overlays = [ ];
  },
  ...
}:
let
  lemonbar = (
    pkgs.lemonbar-xft.overrideAttrs (old: {
      src = pkgs.fetchFromGitHub {
        owner = "drscream";
        repo = "lemonbar-xft";
        rev = "a64a2a6a6d643f4d92f9d7600722710eebce7bdb";
        sha256 = "sha256-T5FhEPIiDt/9paJwL9Sj84CBtA0YFi1hZz0+87Hd6jU=";
        # https://github.com/drscream/lemonbar-xft/pull/2
      };
    })
  );
in
# Tried using pyproject.nix but mpd2 dependency wouldn't resolve,
# is called pyton-mpd2 on PyPi but mpd2 in nixpkgs.
pkgs.python3Packages.buildPythonApplication rec {
  pname = "frobar";
  version = "3.0";

  propagatedBuildInputs = with pkgs.python3Packages; [
    i3ipc
    psutil
    pulsectl-asyncio
    pygobject3
    rich
  ];
  nativeBuildInputs =
    [ lemonbar ]
    ++ (with pkgs; [
      wirelesstools
      playerctl
    ]);
  makeWrapperArgs = [
    "--prefix PATH : ${pkgs.lib.makeBinPath nativeBuildInputs}"
    "--prefix GI_TYPELIB_PATH : ${GI_TYPELIB_PATH}"
  ];

  GI_TYPELIB_PATH = pkgs.lib.makeSearchPath "lib/girepository-1.0" [
    pkgs.glib.out
    pkgs.playerctl
  ];

  src = ./.;
}
