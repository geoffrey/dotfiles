import asyncio
import collections
import datetime
import enum
import logging
import signal
import typing

import gi
import gi.events
import gi.repository.GLib
import i3ipc
import i3ipc.aio
import rich.color
import rich.logging
import rich.terminal_theme

logging.basicConfig(
    level="DEBUG",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[rich.logging.RichHandler()],
)
log = logging.getLogger("frobar")

T = typing.TypeVar("T", bound="ComposableText")
P = typing.TypeVar("P", bound="ComposableText")
C = typing.TypeVar("C", bound="ComposableText")
Sortable = str | int

# Display utilities


def humanSize(numi: int) -> str:
    """
    Returns a string of width 3+3
    """
    num = float(numi)
    for unit in ("B  ", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB"):
        if abs(num) < 1000:
            if num >= 10:
                return f"{int(num):3d}{unit}"
            else:
                return f"{num:.1f}{unit}"
        num /= 1024
    return f"{numi:d}YiB"


def ramp(p: float, states: str = " ▁▂▃▄▅▆▇█") -> str:
    if p < 0:
        return ""
    d, m = divmod(p, 1.0)
    text = states[-1] * int(d)
    if m > 0:
        text += states[round(m * (len(states) - 1))]
    return text


def clip(text: str, length: int = 30) -> str:
    if len(text) > length:
        text = text[: length - 1] + "…"
    return text


class ComposableText(typing.Generic[P, C]):

    def __init__(
        self,
        parent: typing.Optional[P] = None,
        sortKey: Sortable = 0,
    ) -> None:
        self.parent: typing.Optional[P] = None
        self.children: typing.MutableSequence[C] = list()
        self.sortKey = sortKey
        if parent:
            self.setParent(parent)
        self.bar = self.getFirstParentOfType(Bar)

    def setParent(self, parent: P) -> None:
        assert self.parent is None
        parent.children.append(self)
        assert isinstance(parent.children, list)
        parent.children.sort(key=lambda c: c.sortKey)
        self.parent = parent
        self.parent.updateMarkup()

    def unsetParent(self) -> None:
        assert self.parent
        self.parent.children.remove(self)
        self.parent.updateMarkup()
        self.parent = None

    def getFirstParentOfType(self, typ: typing.Type[T]) -> T:
        parent = self
        while not isinstance(parent, typ):
            assert parent.parent, f"{self} doesn't have a parent of {typ}"
            parent = parent.parent
        return parent

    def updateMarkup(self) -> None:
        self.bar.refresh.set()
        # TODO OPTI See if worth caching the output

    def generateMarkup(self) -> str:
        raise NotImplementedError(f"{self} cannot generate markup")

    def getMarkup(self) -> str:
        return self.generateMarkup()


class Button(enum.Enum):
    CLICK_LEFT = "1"
    CLICK_MIDDLE = "2"
    CLICK_RIGHT = "3"
    SCROLL_UP = "4"
    SCROLL_DOWN = "5"


class Section(ComposableText):
    """
    Colorable block separated by chevrons
    """

    def __init__(
        self,
        parent: "Module",
        sortKey: Sortable = 0,
        color: rich.color.Color = rich.color.Color.default(),
    ) -> None:
        super().__init__(parent=parent, sortKey=sortKey)
        self.parent: "Module"
        self.color = color

        self.desiredText: str | None = None
        self.text = ""
        self.targetSize = -1
        self.size = -1
        self.animationTask: asyncio.Task | None = None
        self.actions: dict[Button, str] = dict()

    def isHidden(self) -> bool:
        return self.size < 0

    # Geometric series, with a cap
    ANIM_A = 0.025
    ANIM_R = 0.9
    ANIM_MIN = 0.001

    async def animate(self) -> None:
        increment = 1 if self.size < self.targetSize else -1
        loop = asyncio.get_running_loop()
        frameTime = loop.time()
        animTime = self.ANIM_A
        skipped = 0

        while self.size != self.targetSize:
            self.size += increment
            self.updateMarkup()

            animTime *= self.ANIM_R
            animTime = max(self.ANIM_MIN, animTime)
            frameTime += animTime
            sleepTime = frameTime - loop.time()

            # In case of stress, skip refreshing by not awaiting
            if sleepTime > 0:
                if skipped > 0:
                    log.warning(f"Skipped {skipped} animation frame(s)")
                    skipped = 0
                await asyncio.sleep(sleepTime)
            else:
                skipped += 1

    def setText(self, text: str | None) -> None:
        # OPTI Don't redraw nor reset animation if setting the same text
        if self.desiredText == text:
            return
        self.desiredText = text
        if text is None:
            self.text = ""
            self.targetSize = -1
        else:
            self.text = f" {text} "
            self.targetSize = len(self.text)
        if self.animationTask:
            self.animationTask.cancel()
        # OPTI Skip the whole animation task if not required
        if self.size == self.targetSize:
            self.updateMarkup()
        else:
            self.animationTask = self.bar.taskGroup.create_task(self.animate())

    def setAction(self, button: Button, callback: typing.Callable | None) -> None:
        if button in self.actions:
            command = self.actions[button]
            self.bar.removeAction(command)
            del self.actions[button]
        if callback:
            command = self.bar.addAction(callback)
            self.actions[button] = command

    def generateMarkup(self) -> str:
        assert not self.isHidden()
        pad = max(0, self.size - len(self.text))
        text = self.text[: self.size] + " " * pad
        for button, command in self.actions.items():
            text = "%{A" + button.value + ":" + command + ":}" + text + "%{A}"
        return text


class Module(ComposableText):
    """
    Sections handled by a same updater
    """

    def __init__(self, parent: "Side") -> None:
        super().__init__(parent=parent)
        self.parent: "Side"
        self.children: typing.MutableSequence[Section]

        self.mirroring: Module | None = None
        self.mirrors: list[Module] = list()

    def mirror(self, module: "Module") -> None:
        self.mirroring = module
        module.mirrors.append(self)

    def getSections(self) -> typing.Sequence[Section]:
        if self.mirroring:
            return self.mirroring.children
        else:
            return self.children

    def updateMarkup(self) -> None:
        super().updateMarkup()
        for mirror in self.mirrors:
            mirror.updateMarkup()


class Alignment(enum.Enum):
    LEFT = "l"
    RIGHT = "r"
    CENTER = "c"


class Side(ComposableText):
    def __init__(self, parent: "Screen", alignment: Alignment) -> None:
        super().__init__(parent=parent)
        self.parent: Screen
        self.children: typing.MutableSequence[Module] = []

        self.alignment = alignment
        self.bar = parent.getFirstParentOfType(Bar)

    def generateMarkup(self) -> str:
        if not self.children:
            return ""
        text = "%{" + self.alignment.value + "}"
        lastSection: Section | None = None
        for module in self.children:
            for section in module.getSections():
                if section.isHidden():
                    continue
                hexa = section.color.get_truecolor(theme=self.bar.theme).hex
                if lastSection is None:
                    if self.alignment == Alignment.LEFT:
                        text += "%{B" + hexa + "}%{F-}"
                    else:
                        text += "%{B-}%{F" + hexa + "}%{R}%{F-}"
                elif isinstance(lastSection, SpacerSection):
                    text += "%{B-}%{F" + hexa + "}%{R}%{F-}"
                else:
                    if self.alignment == Alignment.RIGHT:
                        if lastSection.color == section.color:
                            text += ""
                        else:
                            text += "%{F" + hexa + "}%{R}"
                    else:
                        if lastSection.color == section.color:
                            text += ""
                        else:
                            text += "%{R}%{B" + hexa + "}"
                    text += "%{F-}"
                text += section.getMarkup()
                lastSection = section
        if self.alignment != Alignment.RIGHT and lastSection:
            text += "%{R}%{B-}"
        return text


class Screen(ComposableText):
    def __init__(self, parent: "Bar", output: str) -> None:
        super().__init__(parent=parent)
        self.parent: "Bar"
        self.children: typing.MutableSequence[Side]

        self.output = output

        for alignment in Alignment:
            Side(parent=self, alignment=alignment)

    def generateMarkup(self) -> str:
        return ("%{Sn" + self.output + "}") + "".join(
            side.getMarkup() for side in self.children
        )


class Bar(ComposableText):
    """
    Top-level
    """

    def __init__(
        self,
        theme: rich.terminal_theme.TerminalTheme = rich.terminal_theme.DEFAULT_TERMINAL_THEME,
    ) -> None:
        super().__init__()
        self.parent: None
        self.children: typing.MutableSequence[Screen]
        self.longRunningTasks: list[asyncio.Task] = list()
        self.theme = theme

        self.refresh = asyncio.Event()
        self.taskGroup = asyncio.TaskGroup()
        self.providers: list["Provider"] = list()
        self.actionIndex = 0
        self.actions: dict[str, typing.Callable] = dict()

        self.periodicProviderTask: typing.Coroutine | None = None

        i3 = i3ipc.Connection()
        outputs = i3.get_outputs()
        outputs.sort(key=lambda output: output.rect.x)
        for output in outputs:
            if not output.active:
                continue
            Screen(parent=self, output=output.name)

    def addLongRunningTask(self, coro: typing.Coroutine) -> None:
        task = self.taskGroup.create_task(coro)
        self.longRunningTasks.append(task)

    async def run(self) -> None:
        cmd = [
            "lemonbar",
            "-b",
            "-a",
            "64",
            "-f",
            "DejaVuSansM Nerd Font:size=10",
            "-F",
            self.theme.foreground_color.hex,
            "-B",
            self.theme.background_color.hex,
        ]
        proc = await asyncio.create_subprocess_exec(
            *cmd, stdout=asyncio.subprocess.PIPE, stdin=asyncio.subprocess.PIPE
        )

        async def refresher() -> None:
            assert proc.stdin
            while True:
                await self.refresh.wait()
                self.refresh.clear()
                markup = self.getMarkup()
                proc.stdin.write(markup.encode())

        async def actionHandler() -> None:
            assert proc.stdout
            while True:
                line = await proc.stdout.readline()
                command = line.decode().strip()
                callback = self.actions.get(command)
                if callback is None:
                    # In some conditions on start it's empty
                    log.error(f"Unknown command: {command}")
                    return
                callback()

        async with self.taskGroup:
            self.addLongRunningTask(refresher())
            self.addLongRunningTask(actionHandler())
            for provider in self.providers:
                self.addLongRunningTask(provider.run())

            def exit() -> None:
                log.info("Terminating")
                for task in self.longRunningTasks:
                    task.cancel()

            loop = asyncio.get_event_loop()
            loop.add_signal_handler(signal.SIGINT, exit)

    def generateMarkup(self) -> str:
        return "".join(screen.getMarkup() for screen in self.children) + "\n"

    def addProvider(
        self,
        provider: "Provider",
        alignment: Alignment = Alignment.LEFT,
        screenNum: int | None = None,
    ) -> None:
        """
        screenNum: the provider will be added on this screen if set, all otherwise
        """
        modules = list()
        for s, screen in enumerate(self.children):
            if screenNum is None or s == screenNum:
                side = next(filter(lambda s: s.alignment == alignment, screen.children))
                module = Module(parent=side)
                modules.append(module)
        provider.modules = modules
        if modules:
            self.providers.append(provider)

    def addAction(self, callback: typing.Callable) -> str:
        command = f"{self.actionIndex:x}"
        self.actions[command] = callback
        self.actionIndex += 1
        return command

    def removeAction(self, command: str) -> None:
        del self.actions[command]

    def launch(self) -> None:
        # Using GLib's event loop so we can run GLib's code
        policy = gi.events.GLibEventLoopPolicy()
        asyncio.set_event_loop_policy(policy)
        loop = policy.get_event_loop()
        loop.run_until_complete(self.run())


class Provider:
    sectionType: type[Section] = Section

    def __init__(self, color: rich.color.Color = rich.color.Color.default()) -> None:
        self.modules: list[Module] = list()
        self.color = color

    async def run(self) -> None:
        # Not a NotImplementedError, otherwise can't combine all classes
        pass


class MirrorProvider(Provider):
    def __init__(self, color: rich.color.Color = rich.color.Color.default()) -> None:
        super().__init__(color=color)
        self.module: Module

    async def run(self) -> None:
        await super().run()
        self.module = self.modules[0]
        for module in self.modules[1:]:
            module.mirror(self.module)


class SingleSectionProvider(MirrorProvider):
    async def run(self) -> None:
        await super().run()
        self.section = self.sectionType(parent=self.module, color=self.color)


class StaticProvider(SingleSectionProvider):
    def __init__(
        self, text: str, color: rich.color.Color = rich.color.Color.default()
    ) -> None:
        super().__init__(color=color)
        self.text = text

    async def run(self) -> None:
        await super().run()
        self.section.setText(self.text)


class SpacerSection(Section):
    pass


class SpacerProvider(SingleSectionProvider):
    sectionType = SpacerSection

    def __init__(self, length: int = 5) -> None:
        super().__init__(color=rich.color.Color.default())
        self.length = length

    async def run(self) -> None:
        await super().run()
        assert isinstance(self.section, SpacerSection)
        self.section.setText(" " * self.length)


class StatefulSection(Section):

    def __init__(
        self,
        parent: Module,
        sortKey: Sortable = 0,
        color: rich.color.Color = rich.color.Color.default(),
    ) -> None:
        super().__init__(parent=parent, sortKey=sortKey, color=color)
        self.state = 0
        self.numberStates: int

        self.setAction(Button.CLICK_LEFT, self.incrementState)
        self.setAction(Button.CLICK_RIGHT, self.decrementState)

    def incrementState(self) -> None:
        self.state += 1
        self.changeState()

    def decrementState(self) -> None:
        self.state -= 1
        self.changeState()

    def setChangedState(self, callback: typing.Callable) -> None:
        self.callback = callback

    def changeState(self) -> None:
        self.state %= self.numberStates
        self.bar.taskGroup.create_task(self.callback())


class StatefulSectionProvider(Provider):
    sectionType = StatefulSection


class SingleStatefulSectionProvider(StatefulSectionProvider, SingleSectionProvider):
    section: StatefulSection


class MultiSectionsProvider(Provider):

    def __init__(self, color: rich.color.Color = rich.color.Color.default()) -> None:
        super().__init__(color=color)
        self.sectionKeys: dict[Module, dict[Sortable, Section]] = (
            collections.defaultdict(dict)
        )
        self.updaters: dict[Section, typing.Callable] = dict()

    async def getSectionUpdater(self, section: Section) -> typing.Callable:
        raise NotImplementedError()

    @staticmethod
    async def doNothing() -> None:
        pass

    async def updateSections(self, sections: set[Sortable], module: Module) -> None:
        moduleSections = self.sectionKeys[module]
        async with asyncio.TaskGroup() as tg:
            for sortKey in sections:
                section = moduleSections.get(sortKey)
                if not section:
                    section = self.sectionType(
                        parent=module, sortKey=sortKey, color=self.color
                    )
                    self.updaters[section] = await self.getSectionUpdater(section)
                moduleSections[sortKey] = section

                updater = self.updaters[section]
                tg.create_task(updater())

            missingKeys = set(moduleSections.keys()) - sections
            for missingKey in missingKeys:
                section = moduleSections.get(missingKey)
                assert section
                section.setText(None)


class PeriodicProvider(Provider):
    async def init(self) -> None:
        pass

    async def loop(self) -> None:
        raise NotImplementedError()

    @classmethod
    async def task(cls, bar: Bar) -> None:
        providers = list()
        for provider in bar.providers:
            if isinstance(provider, PeriodicProvider):
                providers.append(provider)
                await provider.init()

        while True:
            # TODO Block bar update during the periodic update of the loops
            loops = [provider.loop() for provider in providers]
            asyncio.gather(*loops)

            now = datetime.datetime.now()
            # Hardcoded to 1 second... not sure if we want some more than that,
            # and if the logic to check if a task should run would be a win
            # compared to the task itself
            remaining = 1 - now.microsecond / 1000000
            await asyncio.sleep(remaining)

    async def run(self) -> None:
        await super().run()
        for module in self.modules:
            bar = module.getFirstParentOfType(Bar)
        assert bar
        if not bar.periodicProviderTask:
            bar.periodicProviderTask = PeriodicProvider.task(bar)
            bar.addLongRunningTask(bar.periodicProviderTask)


class PeriodicStatefulProvider(SingleStatefulSectionProvider, PeriodicProvider):
    async def run(self) -> None:
        await super().run()
        self.section.setChangedState(self.loop)


class AlertingProvider(Provider):
    COLOR_NORMAL = rich.color.Color.parse("green")
    COLOR_WARNING = rich.color.Color.parse("yellow")
    COLOR_DANGER = rich.color.Color.parse("red")

    warningThreshold: float
    dangerThreshold: float

    def updateLevel(self, level: float) -> None:
        if level > self.dangerThreshold:
            color = self.COLOR_DANGER
        elif level > self.warningThreshold:
            color = self.COLOR_WARNING
        else:
            color = self.COLOR_NORMAL
        for module in self.modules:
            for section in module.getSections():
                section.color = color
