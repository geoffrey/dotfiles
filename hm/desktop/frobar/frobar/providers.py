import asyncio
import collections
import datetime
import ipaddress
import os
import socket
import time
import typing

import gi
import i3ipc
import i3ipc.aio
import psutil
import pulsectl
import pulsectl_asyncio
import rich.color

from frobar.common import (AlertingProvider, Button, MirrorProvider, Module,
                           MultiSectionsProvider, PeriodicProvider,
                           PeriodicStatefulProvider, Screen, Section,
                           SingleSectionProvider, StatefulSection,
                           StatefulSectionProvider, clip, humanSize, log, ramp)

gi.require_version("Playerctl", "2.0")
import gi.repository.Playerctl


class I3ModeProvider(SingleSectionProvider):
    def on_mode(self, i3: i3ipc.Connection, e: i3ipc.Event) -> None:
        self.section.setText(None if e.change == "default" else e.change)

    async def run(self) -> None:
        await super().run()
        i3 = await i3ipc.aio.Connection(auto_reconnect=True).connect()
        i3.on(i3ipc.Event.MODE, self.on_mode)
        await i3.main()

    # TODO Hide WorkspaceProvider when this is active


class I3WindowTitleProvider(SingleSectionProvider):
    # TODO FEAT To make this available from start, we need to find the
    # `focused=True` element following the `focus` array
    def on_window(self, i3: i3ipc.Connection, e: i3ipc.Event) -> None:
        if e.container.name is None:
            self.section.setText(None)
        else:
            self.section.setText(clip(e.container.name, 60))

    async def run(self) -> None:
        await super().run()
        i3 = await i3ipc.aio.Connection(auto_reconnect=True).connect()
        i3.on(i3ipc.Event.WINDOW, self.on_window)
        await i3.main()


class I3WorkspacesProvider(MultiSectionsProvider):
    COLOR_URGENT = rich.color.Color.parse("red")
    COLOR_FOCUSED = rich.color.Color.parse("yellow")
    # TODO Should be orange (not a terminal color)
    COLOR_VISIBLE = rich.color.Color.parse("cyan")
    COLOR_DEFAULT = rich.color.Color.parse("bright_black")

    def __init__(
        self,
        custom_names: dict[str, str] = {},
    ) -> None:
        super().__init__()
        self.workspaces: dict[int, i3ipc.WorkspaceReply]
        self.custom_names = custom_names

        self.modulesFromOutput: dict[str, Module] = dict()

    async def getSectionUpdater(self, section: Section) -> typing.Callable:
        assert isinstance(section.sortKey, int)
        num = section.sortKey

        def switch_to_workspace() -> None:
            self.bar.taskGroup.create_task(self.i3.command(f"workspace number {num}"))

        section.setAction(Button.CLICK_LEFT, switch_to_workspace)

        async def update() -> None:
            workspace = self.workspaces.get(num)
            if workspace is None:
                log.warning(f"Can't find workspace {num}")
                section.setText("X")
                return

            name = workspace.name
            if workspace.urgent:
                section.color = self.COLOR_URGENT
            elif workspace.focused:
                section.color = self.COLOR_FOCUSED
            elif workspace.visible:
                section.color = self.COLOR_VISIBLE
            else:
                section.color = self.COLOR_DEFAULT
            if workspace.focused:
                name = self.custom_names.get(name, name)
            section.setText(name)

        return update

    async def updateWorkspaces(self) -> None:
        """
        Since the i3 IPC interface cannot really tell you by events
        when workspaces get invisible or not urgent anymore.
        Relying on those exclusively would require reimplementing some of i3 logic.
        Fetching all the workspaces on event looks ugly but is the most maintainable.
        Times I tried to challenge this and failed: 2.
        """
        workspaces = await self.i3.get_workspaces()
        self.workspaces = dict()
        modules = collections.defaultdict(set)
        for workspace in workspaces:
            self.workspaces[workspace.num] = workspace
            module = self.modulesFromOutput[workspace.output]
            modules[module].add(workspace.num)

        await asyncio.gather(
            *[self.updateSections(nums, module) for module, nums in modules.items()]
        )

    def onWorkspaceChange(
        self, i3: i3ipc.Connection, e: i3ipc.Event | None = None
    ) -> None:
        # Cancelling the task doesn't seem to prevent performance double-events
        self.bar.taskGroup.create_task(self.updateWorkspaces())

    async def run(self) -> None:
        for module in self.modules:
            screen = module.getFirstParentOfType(Screen)
            output = screen.output
            self.modulesFromOutput[output] = module
            self.bar = module.bar

        self.i3 = await i3ipc.aio.Connection(auto_reconnect=True).connect()
        self.i3.on(i3ipc.Event.WORKSPACE, self.onWorkspaceChange)
        self.onWorkspaceChange(self.i3)
        await self.i3.main()


class MprisProvider(MirrorProvider):

    STATUSES = {
        gi.repository.Playerctl.PlaybackStatus.PLAYING: "",
        gi.repository.Playerctl.PlaybackStatus.PAUSED: "",
        gi.repository.Playerctl.PlaybackStatus.STOPPED: "",
    }

    PROVIDERS = {
        "mpd": "",
        "firefox": "",
        "chromium": "",
        "mpv": "",
    }

    async def run(self) -> None:
        await super().run()
        self.status = self.sectionType(parent=self.module, color=self.color)
        self.album = self.sectionType(parent=self.module, color=self.color)
        self.artist = self.sectionType(parent=self.module, color=self.color)
        self.title = self.sectionType(parent=self.module, color=self.color)

        self.manager = gi.repository.Playerctl.PlayerManager()
        self.manager.connect("name-appeared", self.on_name_appeared)
        self.manager.connect("player-vanished", self.on_player_vanished)

        self.playerctldName = gi.repository.Playerctl.PlayerName()
        self.playerctldName.name = "playerctld"
        self.playerctldName.source = gi.repository.Playerctl.Source.DBUS_SESSION

        self.player: gi.repository.Playerctl.Player | None = None
        self.playing = asyncio.Event()

        for name in self.manager.props.player_names:
            self.init_player(name)

        self.updateSections()

        while True:
            # Occasionally it will skip a second
            # but haven't managed to reproduce with debug info
            await self.playing.wait()
            self.updateTitle()
            if self.player:
                pos = self.player.props.position
                rem = 1 - (pos % 1000000) / 1000000
                await asyncio.sleep(rem)
            else:
                self.playing.clear()

    @staticmethod
    def get(
        something: gi.overrides.GLib.Variant, key: str, default: typing.Any = None
    ) -> typing.Any:
        if key in something.keys():
            return something[key]
        else:
            return default

    @staticmethod
    def formatUs(ms: int) -> str:
        if ms < 60 * 60 * 1000000:
            return time.strftime("%M:%S", time.gmtime(ms // 1000000))
        else:
            return str(datetime.timedelta(microseconds=ms))

    def findCurrentPlayer(self) -> None:
        for name in [self.playerctldName] + self.manager.props.player_names:
            # TODO Test what happens when playerctld is not available
            self.player = gi.repository.Playerctl.Player.new_from_name(name)
            if not self.player.props.can_play:
                continue
            break
        else:
            self.player = None

    def updateSections(self) -> None:
        self.findCurrentPlayer()

        if self.player is None:
            self.status.setText(None)
            self.album.setText(None)
            self.artist.setText(None)
            self.title.setText(None)
            self.playing.clear()
            return

        player = self.player.props.player_name
        player = self.PROVIDERS.get(player, player)
        status = self.STATUSES.get(self.player.props.playback_status, "?")
        self.status.setText(f"{player} {status}")

        if (
            self.player.props.playback_status
            == gi.repository.Playerctl.PlaybackStatus.PLAYING
        ):
            self.playing.set()
        else:
            self.playing.clear()

        metadata = self.player.props.metadata

        album = self.get(metadata, "xesam:album")
        if album:
            self.album.setText(f" {clip(album)}")
        else:
            self.album.setText(None)

        artists = self.get(metadata, "xesam:artist")
        if artists:
            artist = ", ".join(artists)
            self.artist.setText(f" {clip(artist)}")
        else:
            self.artist.setText(None)

        self.updateTitle()

    def updateTitle(self) -> None:
        if self.player is None:
            return

        metadata = self.player.props.metadata
        pos = self.player.props.position  # In µs
        text = f" {self.formatUs(pos)}"
        dur = self.get(metadata, "mpris:length")
        if dur:
            text += f"/{self.formatUs(dur)}"
        title = self.get(metadata, "xesam:title")
        if title:
            text += f" {clip(title)}"
        self.title.setText(text)

    def on_player_vanished(
        self,
        manager: gi.repository.Playerctl.PlayerManager,
        player: gi.repository.Playerctl.Player,
    ) -> None:
        self.updateSections()

    def on_event(
        self,
        player: gi.repository.Playerctl.Player,
        _: typing.Any,
        manager: gi.repository.Playerctl.PlayerManager,
    ) -> None:
        self.updateSections()

    def init_player(self, name: gi.repository.Playerctl.PlayerName) -> None:
        player = gi.repository.Playerctl.Player.new_from_name(name)
        # All events will cause the active player to change,
        # so we listen on all events, even if the display won't change
        player.connect("playback-status", self.on_event, self.manager)
        player.connect("loop-status", self.on_event, self.manager)
        player.connect("shuffle", self.on_event, self.manager)
        player.connect("metadata", self.on_event, self.manager)
        player.connect("volume", self.on_event, self.manager)
        player.connect("seeked", self.on_event, self.manager)
        self.manager.manage_player(player)

    def on_name_appeared(
        self, manager: gi.repository.Playerctl.PlayerManager, name: str
    ) -> None:
        self.init_player(name)
        self.updateSections()


class CpuProvider(AlertingProvider, PeriodicStatefulProvider):
    async def init(self) -> None:
        self.section.numberStates = 3
        self.warningThreshold = 75
        self.dangerThreshold = 95

    async def loop(self) -> None:
        percent = psutil.cpu_percent(percpu=False)
        self.updateLevel(percent)

        text = ""
        if self.section.state >= 2:
            percents = psutil.cpu_percent(percpu=True)
            text += " " + "".join([ramp(p / 100) for p in percents])
        elif self.section.state >= 1:
            text += " " + ramp(percent / 100)
        self.section.setText(text)


class LoadProvider(AlertingProvider, PeriodicStatefulProvider):
    async def init(self) -> None:
        self.section.numberStates = 3
        self.warningThreshold = 5
        self.dangerThreshold = 10

    async def loop(self) -> None:
        load = os.getloadavg()
        self.updateLevel(load[0])

        text = ""
        loads = 3 if self.section.state >= 2 else self.section.state
        for load_index in range(loads):
            text += f" {load[load_index]:.2f}"
        self.section.setText(text)


class RamProvider(AlertingProvider, PeriodicStatefulProvider):

    async def init(self) -> None:
        self.section.numberStates = 4
        self.warningThreshold = 75
        self.dangerThreshold = 95

    async def loop(self) -> None:
        mem = psutil.virtual_memory()
        self.updateLevel(mem.percent)

        text = ""
        if self.section.state >= 1:
            text += " " + ramp(mem.percent / 100)
            if self.section.state >= 2:
                text += humanSize(mem.total - mem.available)
                if self.section.state >= 3:
                    text += "/" + humanSize(mem.total)
        self.section.setText(text)


class TemperatureProvider(AlertingProvider, PeriodicStatefulProvider):
    RAMP = ""
    MAIN_TEMPS = ["coretemp", "amdgpu", "cpu_thermal"]
    # For Intel, AMD and ARM respectively.

    main: str

    async def init(self) -> None:
        self.section.numberStates = 2

        allTemp = psutil.sensors_temperatures()
        for main in self.MAIN_TEMPS:
            if main in allTemp:
                self.main = main
                break
        else:
            raise IndexError("Could not find suitable temperature sensor")

        temp = allTemp[self.main][0]
        self.warningThreshold = temp.high or 90.0
        self.dangerThreshold = temp.critical or 100.0

    async def loop(self) -> None:
        allTemp = psutil.sensors_temperatures()
        temp = allTemp[self.main][0]
        self.updateLevel(temp.current)

        text = ramp(temp.current / self.warningThreshold, self.RAMP)
        if self.section.state >= 1:
            text += f" {temp.current:.0f}°C"
        self.section.setText(text)


class BatteryProvider(AlertingProvider, PeriodicStatefulProvider):
    # TODO Support ACPID for events
    RAMP = ""

    async def init(self) -> None:
        self.section.numberStates = 3
        # TODO 1 refresh rate is too quick

        self.warningThreshold = 75
        self.dangerThreshold = 95

    async def loop(self) -> None:
        bat = psutil.sensors_battery()
        if not bat:
            self.section.setText(None)

        self.updateLevel(100 - bat.percent)

        text = "" if bat.power_plugged else ""
        text += ramp(bat.percent / 100, self.RAMP)

        if self.section.state >= 1:
            text += f" {bat.percent:.0f}%"
            if self.section.state >= 2:
                h = int(bat.secsleft / 3600)
                m = int((bat.secsleft - h * 3600) / 60)
                text += f" ({h:d}:{m:02d})"

        self.section.setText(text)


class PulseaudioProvider(
    MirrorProvider, StatefulSectionProvider, MultiSectionsProvider
):
    async def getSectionUpdater(self, section: Section) -> typing.Callable:
        assert isinstance(section, StatefulSection)
        assert isinstance(section.sortKey, str)

        sink = self.sinks[section.sortKey]

        icon = "?"

        if sink.port_active is None:
            pass
        elif (
            sink.port_active.name == "analog-output-headphones"
            or sink.port_active.description == "Headphones"
        ):
            icon = ""
        elif (
            sink.port_active.name == "analog-output-speaker"
            or sink.port_active.description == "Speaker"
        ):
            icon = ""
        elif sink.port_active.name in ("headset-output", "headphone-output"):
            icon = ""

        section.numberStates = 3
        section.state = 1

        # TODO Change volume with wheel

        async def updater() -> None:
            assert isinstance(section, StatefulSection)
            text = icon
            sink = self.sinks[section.sortKey]

            async with pulsectl_asyncio.PulseAsync("frobar-get-volume") as pulse:
                vol = await pulse.volume_get_all_chans(sink)
            if section.state == 1:
                text += f" {ramp(vol)}"
            elif section.state == 2:
                text += f" {vol:.0%}"
            # TODO Show which is default
            section.setText(text)

        section.setChangedState(updater)

        return updater

    async def update(self) -> None:
        async with pulsectl_asyncio.PulseAsync("frobar-list-sinks") as pulse:
            self.sinks = dict((sink.name, sink) for sink in await pulse.sink_list())
            await self.updateSections(set(self.sinks.keys()), self.module)

    async def run(self) -> None:
        await super().run()
        await self.update()
        async with pulsectl_asyncio.PulseAsync("frobar-events-listener") as pulse:
            async for event in pulse.subscribe_events(pulsectl.PulseEventMaskEnum.sink):
                await self.update()


class NetworkProvider(
    MirrorProvider, PeriodicProvider, StatefulSectionProvider, MultiSectionsProvider
):
    def __init__(
        self,
        color: rich.color.Color = rich.color.Color.default(),
    ) -> None:
        super().__init__(color=color)

    async def init(self) -> None:
        loop = asyncio.get_running_loop()
        self.time = loop.time()
        self.io_counters = psutil.net_io_counters(pernic=True)

    @staticmethod
    def getIfaceAttributes(iface: str) -> tuple[bool, str, bool]:
        relevant = True
        icon = "?"
        wifi = False
        if iface == "lo":
            relevant = False
        elif iface.startswith("eth") or iface.startswith("enp"):
            if "u" in iface:
                icon = ""
            else:
                icon = ""
        elif iface.startswith("wlan") or iface.startswith("wl"):
            icon = ""
            wifi = True
        elif (
            iface.startswith("tun") or iface.startswith("tap") or iface.startswith("wg")
        ):
            icon = ""

        elif iface.startswith("docker"):
            icon = ""
        elif iface.startswith("veth"):
            icon = ""
        elif iface.startswith("vboxnet"):
            icon = ""

        return relevant, icon, wifi

    async def getSectionUpdater(self, section: Section) -> typing.Callable:

        assert isinstance(section, StatefulSection)
        assert isinstance(section.sortKey, str)
        iface = section.sortKey

        relevant, icon, wifi = self.getIfaceAttributes(iface)

        if not relevant:
            return self.doNothing

        section.numberStates = 5 if wifi else 4
        section.state = 1 if wifi else 0

        async def update() -> None:
            assert isinstance(section, StatefulSection)

            if not self.if_stats[iface].isup:
                section.setText(None)
                return

            text = icon

            state = section.state + (0 if wifi else 1)
            if wifi and state >= 1:  # SSID
                cmd = ["iwgetid", iface, "--raw"]
                proc = await asyncio.create_subprocess_exec(
                    *cmd, stdout=asyncio.subprocess.PIPE
                )
                stdout, stderr = await proc.communicate()
                text += f" {stdout.decode().strip()}"

            if state >= 2:  # Address
                for address in self.if_addrs[iface]:
                    if address.family == socket.AF_INET:
                        net = ipaddress.IPv4Network(
                            (address.address, address.netmask), strict=False
                        )
                        text += f" {address.address}/{net.prefixlen}"
                        break

            if state >= 3:  # Speed
                prevRecv = self.prev_io_counters[iface].bytes_recv
                recv = self.io_counters[iface].bytes_recv
                prevSent = self.prev_io_counters[iface].bytes_sent
                sent = self.io_counters[iface].bytes_sent
                dt = self.time - self.prev_time

                recvDiff = (recv - prevRecv) / dt
                sentDiff = (sent - prevSent) / dt
                text += f" ↓{humanSize(recvDiff)}↑{humanSize(sentDiff)}"

            if state >= 4:  # Counter
                text += f" ⇓{humanSize(recv)}⇑{humanSize(sent)}"

            section.setText(text)

        section.setChangedState(update)

        return update

    async def loop(self) -> None:
        loop = asyncio.get_running_loop()

        self.prev_io_counters = self.io_counters
        self.prev_time = self.time
        # On-demand would only benefit if_addrs:
        # stats are used to determine display,
        # and we want to keep previous io_counters
        # so displaying stats is ~instant.
        self.time = loop.time()
        self.if_stats = psutil.net_if_stats()
        self.if_addrs = psutil.net_if_addrs()
        self.io_counters = psutil.net_io_counters(pernic=True)

        await self.updateSections(set(self.if_stats.keys()), self.module)


class TimeProvider(PeriodicStatefulProvider):
    FORMATS = ["%H:%M", "%m-%d %H:%M:%S", "%a %y-%m-%d %H:%M:%S"]

    async def init(self) -> None:
        self.section.state = 1
        self.section.numberStates = len(self.FORMATS)

    async def loop(self) -> None:
        now = datetime.datetime.now()
        format = self.FORMATS[self.section.state]
        self.section.setText(now.strftime(format))
