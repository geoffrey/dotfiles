import rich.color
import rich.logging
import rich.terminal_theme

import frobar.common
import frobar.providers
from frobar.common import Alignment


def main() -> None:
    # TODO Configurable
    FROGARIZED = [
        "#092c0e",
        "#143718",
        "#5a7058",
        "#677d64",
        "#89947f",
        "#99a08d",
        "#fae2e3",
        "#fff0f1",
        "#e0332e",
        "#cf4b15",
        "#bb8801",
        "#8d9800",
        "#1fa198",
        "#008dd1",
        "#5c73c4",
        "#d43982",
    ]
    # TODO Not super happy with the color management,
    # while using an existing library is great, it's limited to ANSI colors

    def base16_color(color: int) -> tuple[int, int, int]:
        hexa = FROGARIZED[color]
        return tuple(rich.color.parse_rgb_hex(hexa[1:]))

    theme = rich.terminal_theme.TerminalTheme(
        base16_color(0x0),
        base16_color(0x0),  # TODO should be 7, currently 0 so it's compatible with v2
        [
            base16_color(0x0),  # black
            base16_color(0x8),  # red
            base16_color(0xB),  # green
            base16_color(0xA),  # yellow
            base16_color(0xD),  # blue
            base16_color(0xE),  # magenta
            base16_color(0xC),  # cyan
            base16_color(0x5),  # white
        ],
        [
            base16_color(0x3),  # bright black
            base16_color(0x8),  # bright red
            base16_color(0xB),  # bright green
            base16_color(0xA),  # bright yellow
            base16_color(0xD),  # bright blue
            base16_color(0xE),  # bright magenta
            base16_color(0xC),  # bright cyan
            base16_color(0x7),  # bright white
        ],
    )

    bar = frobar.common.Bar(theme=theme)
    dualScreen = len(bar.children) > 1
    leftPreferred = 0 if dualScreen else None
    rightPreferred = 1 if dualScreen else None

    workspaces_suffixes = "▲■"
    workspaces_names = dict(
        (str(i + 1), f"{i+1} {c}") for i, c in enumerate(workspaces_suffixes)
    )

    color = rich.color.Color.parse

    bar.addProvider(
        frobar.providers.I3ModeProvider(color=color("red")), alignment=Alignment.LEFT
    )
    bar.addProvider(
        frobar.providers.I3WorkspacesProvider(custom_names=workspaces_names),
        alignment=Alignment.LEFT,
    )

    if dualScreen:
        bar.addProvider(
            frobar.providers.I3WindowTitleProvider(color=color("white")),
            screenNum=0,
            alignment=Alignment.CENTER,
        )
        bar.addProvider(
            frobar.providers.MprisProvider(color=color("bright_white")),
            screenNum=rightPreferred,
            alignment=Alignment.CENTER,
        )
    else:
        bar.addProvider(
            frobar.common.SpacerProvider(),
            alignment=Alignment.LEFT,
        )
        bar.addProvider(
            frobar.providers.MprisProvider(color=color("bright_white")),
            alignment=Alignment.LEFT,
        )

    bar.addProvider(
        frobar.providers.CpuProvider(),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.LoadProvider(),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.RamProvider(),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.TemperatureProvider(),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.BatteryProvider(),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.PulseaudioProvider(color=color("magenta")),
        screenNum=rightPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.NetworkProvider(color=color("blue")),
        screenNum=leftPreferred,
        alignment=Alignment.RIGHT,
    )
    bar.addProvider(
        frobar.providers.TimeProvider(color=color("cyan")), alignment=Alignment.RIGHT
    )

    bar.launch()


if __name__ == "__main__":
    main()
