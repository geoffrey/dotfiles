{
  pkgs,
  lib,
  config,
  ...
}:
let
  # lockColors = with config.lib.stylix.colors.withHashtag; { a = base00; b = base01; d = base00; }; # Black or White, depending on current theme
  # lockColors = with config.lib.stylix.colors.withHashtag; { a = base0A; b = base0B; d = base00; }; # Green + Yellow
  lockColors = {
    a = "#82a401";
    b = "#466c01";
    d = "#648901";
  }; # Old
  lockSvg = pkgs.writeText "lock.svg" ''
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" height="50" width="50">
      <path fill="${lockColors.a}" d="M0 50h50V0H0z"/>
      <path d="M0 0l50 50H25L0 25zm50 0v25L25 0z" fill="${lockColors.b}"/>
    </svg>
  '';
  lockPng = pkgs.runCommand "lock.png" { } "${pkgs.imagemagick}/bin/convert ${lockSvg} $out";
  mod = config.xsession.windowManager.i3.config.modifier;
  xautolockState = "${config.xdg.cacheHome}/xautolock";
in
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    home.packages = [
      (pkgs.writeShellApplication {
        name = "xlock";
        text = ''
          ${config.frogeye.hooks.lock}
          # TODO Reevaluate whether we want this or not
          if ! ${pkgs.lightdm}/bin/dm-tool lock
          then
              if [ -d ${config.xdg.cacheHome}/lockpatterns ]
              then
                  pattern=$(${pkgs.findutils} ${config.xdg.cacheHome}/lockpatterns | sort -R | head -1)
              else
                  pattern=${lockPng}
              fi
              revert() {
                  ${pkgs.xorg.xset}/bin/xset dpms 0 0 0
              }
              trap revert SIGHUP SIGINT SIGTERM
              ${pkgs.xorg.xset}/bin/xset dpms 5 5 5
              ${pkgs.i3lock}/bin/i3lock --nofork --color ${
                builtins.substring 1 6 lockColors.d
              } --image="$pattern" --tiling --ignore-empty-password
              revert
          fi
        '';
      })
    ];
    xsession.windowManager.i3.config = {
      keybindings = {
        # Screen off commands
        "${mod}+F1" = "--release exec --no-startup-id ${pkgs.xorg.xset}/bin/xset dpms force off";
        # Toggle to save on buttons
        # xautolock -toggle doesn't allow to read state.
        # Writing into a file also allows frobar to display a lock icon
        "${mod}+F5" = "exec --no-startup-id ${pkgs.writeShellScript "xautolock-toggle" ''
          state="$(cat "${xautolockState}")"
          if [ "$state" = "disabled" ]
          then
            ${pkgs.xautolock}/bin/xautolock -enable
            echo enabled > ${xautolockState}
          else
            ${pkgs.xautolock}/bin/xautolock -disable
            echo disabled > ${xautolockState}
          fi
        ''}";
      };
      startup = [
        # Stop screen after 10 minutes, 1 minutes after lock it
        {
          notification = false;
          command = "${pkgs.writeShellScript "xautolock-start" ''
            echo enabled > ${xautolockState}
            ${pkgs.xautolock}/bin/xautolock -time 10 -locker '${pkgs.xorg.xset}/bin/xset dpms force standby' -killtime 1 -killer xlock
          ''}";
        }
        #  services.screen-locker.xautolock is hardcoded to use systemd for -locker (doesn't even work...)
      ];
    };
  };
}
