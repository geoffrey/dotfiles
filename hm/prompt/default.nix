{
  ...
}:
{
  config = {
    programs.powerline-go = {
      enable = true;
      modules = [
        "user"
        "host"
        "direnv"
        "cwd"
        "perms"
        "nix-shell"
        "venv"
        "git"
      ];
      modulesRight = [
        "jobs"
        "exit"
        "duration"
      ];
      settings = {
        colorize-hostname = true;
        hostname-only-if-ssh = true;
        max-width = 25;
        cwd-max-dir-size = 10;
        duration = "$( test -n \"$__TIMER\" && echo $(( $EPOCHREALTIME - $\{__TIMER:-EPOCHREALTIME})) || echo 0 )";
        # UPST Implement this properly in home-manager, would allow for bash support
      };
      extraUpdatePS1 = ''
        unset __TIMER
        echo -en "\033]0; $USER@$HOST $PWD\007"
      '';
    };
  };
}
