{
  pkgs,
  ...
}:
let
  # UPST
  vim-shot-f = pkgs.vimUtils.buildVimPlugin {
    pname = "vim-shot-f";
    version = "2016-02-05";
    src = pkgs.fetchFromGitHub {
      owner = "deris";
      repo = "vim-shot-f";
      rev = "eea71d2a1038aa87fe175de9150b39dc155e5e7f";
      sha256 = "iAPvIs/lhW+w5kFTZKaY97D/kfCGtqKrJVFvZ8cHu+c=";
    };
    meta.homepage = "https://github.com/deris/vim-shot-f";
  };
  quick-scope = pkgs.vimUtils.buildVimPlugin rec {
    pname = "quick-scope";
    version = "2.6.1";
    src = pkgs.fetchFromGitHub {
      owner = "unblevable";
      repo = "quick-scope";
      rev = "v${version}";
      sha256 = "TcA4jZIdnQd06V+JrXGiCMr0Yhm9gB6OMiTSdzMt/Qw=";
    };
    meta.homepage = "https://github.com/unblevable/quick-scope";
  };
in
{
  config = {
    programs.nixvim = {
      extraPlugins = [
        # f/F mode
        vim-shot-f # Highlight relevant characters for f/F/t/T modes
        quick-scope # Highlight relevant characters for f/F modes one per word but always
      ];
      opts = {
        # From https://www.hillelwayne.com/post/intermediate-vim/
        scrolloff = 10;
        lazyredraw = true; # Do not redraw screen in the middle of a macro. Makes them complete faster.
        cursorcolumn = true;

        # From http://stackoverflow.com/a/5004785/2766106
        list = true;
        listchars = "tab:╾╌,trail:·,extends:↦,precedes:↤,nbsp:_";
        showbreak = "↪";
      };
      plugins = {
        # Underline all instances of the underlined word
        cursorline = {
          enable = true;
          cursorline.enable = false;
        };

        # Catches attention when cursor changed position
        specs = {
          enable = true;
          settings = {
            min_jump = 5;
            popup.fader = "require('specs').pulse_fader";
          };
        };

        # Treesitter
        treesitter = {
          # Allows for better syntax highlighting
          enable = true;
          settings = {
            incremental_selection.enable = true;
            # indent = true; # Not very working last time I tried apparently
          };
        };
        indent-blankline.enable = true; # Show indent guides
        rainbow-delimiters.enable = true; # Randomly colore paired brackets

        nvim-colorizer.enable = true; # Display colors of color-codes
      };
    };
  };
}
