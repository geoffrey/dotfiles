{
  ...
}:
{
  config = {
    programs.nixvim = {
      keymaps = [
        {
          mode = [ "n" ];
          key = "gG";
          action = ''<cmd>lua require"gitlinker".get_buf_range_url("n")<cr>'';
        }
        {
          mode = [ "v" ];
          key = "gG";
          action = ''<cmd>lua require"gitlinker".get_buf_range_url("v")<cr>'';
        }
      ];
      plugins = {
        fugitive.enable = true; # Git basics
        gitsigns.enable = true; # Show changed lines in the gutter
        gitlinker = {
          enable = true; # Copy URL of forge corresponding to cursor
          callbacks = {
            "code.techinc.nl" = "get_gitea_type_url";
            "git.frogeye.fr" = "get_gitea_type_url";
          };
          mappings = null; # This option doesn't work, hence manual mappings
        };
      };
    };
  };
}
