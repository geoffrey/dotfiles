{
  pkgs,
  lib,
  config,
  nixvim,
  ...
}:
{
  # config = lib.mkIf config.programs.nixvim.enable { # Somehow this is infinite recursion?
  config = {
    home.sessionVariables =
      {
        EDITOR = "nvim";
      }
      // lib.optionalAttrs config.frogeye.desktop.xorg {
        VISUAL = "nvim";
      };
    programs.bash.shellAliases = {
      vi = "nvim";
      vim = "nvim";
    };
    programs.nixvim = {
      colorschemes.base16.setUpBar = false; # We want the custom theme for lualine, probably
      opts = {
        ignorecase = true;
        smartcase = true;
        gdefault = true;

        tabstop = 4;
        shiftwidth = 4;
        expandtab = true;

        splitbelow = true;
        visualbell = true;

        updatetime = 250;
        undofile = true;

        wildmode = "longest:full,full";
        wildmenu = true;
      };
      globals = {
        netrw_fastbrowse = 0; # Close the file explorer once you select a file
      };
      plugins = {
        # Go to whatever
        telescope = {
          enable = true;
          keymaps = {
            gF = "find_files";
            gf = "git_files";
            gB = "buffers";
            gl = "current_buffer_fuzzy_find";
            gL = "live_grep";
            gT = "tags";
            gt = "treesitter";
            gm = "marks";
            gh = "oldfiles";
            gH = "command_history";
            gS = "search_history";
            gC = "commands";
          };
          settings = {
            defaults = {
              vimgrep_arguments = [
                "${pkgs.ripgrep}/bin/rg"
                "--color=never"
                "--no-heading"
                "--with-filename"
                "--line-number"
                "--column"
                "--smart-case"
              ];
            };
          };
          extensions.fzf-native = {
            enable = true;
            settings = {
              case_mode = "smart_case";
              fuzzy = true;
              override_file_sorter = true;
              override_generic_sorter = true;
            };
          };
        };
        # TODO Go to any symbol in the current working directory, for when LSP doesn't support it.
        # Or at least something to live_grep the curent word.

        # Surrounding pairs
        vim-surround.enable = true; # Change surrounding pairs (e.g. brackets, quotes)

        undotree.enable = true; # Navigate edition history

      };
      extraPlugins =
        with pkgs.vimPlugins;
        [
          # Search/replace
          vim-abolish # Regex for words, with case in mind
          vim-easy-align # Aligning lines around a certain character

          # Surrounding pairs
          targets-vim # Better interaction with surrounding pairs

          # Language-specific
          tcomment_vim # Language-aware (un)commenting
        ]
        ++ lib.optionals config.frogeye.dev.ansible [
          ansible-vim
          # Doesn't generate snippets, but those are for UltiSnip anyways
        ];
      extraConfigLuaPre = lib.mkBefore ''
        -- If terminal is detected to be light, background will be changed after base16 theme is applied.
        -- Setting manually early to prevent hightlights having weird colors.
        -- https://github.com/RRethy/base16-nvim/issues/77
        vim.opt.background = "${config.stylix.polarity}";
      '';
      extraConfigVim = ''
        " Avoid showing message extra message when using completion
        set shortmess+=c
      '';

      userCommands = {
        Reload = {
          command = "source ${config.xdg.configHome}/nvim/init.lua";
        };
      };

      keymaps = [
        # GENERAL

        # Allow saving of files as sudo when I forgot to start vim using sudo.
        # From https://stackoverflow.com/a/7078429
        {
          mode = "c";
          key = "w!!";
          action = "w !sudo tee > /dev/null %";
        }

        {
          mode = "i";
          key = "jk";
          action = "<Esc>";
        }
        {
          mode = "v";
          key = "<Enter>";
          action = "<Esc>";
        }
        {
          key = "<Enter>";
          action = "o<Esc>";
        }

        # { key = "<C-H>"; action = ":bp<CR>"; }
        # { key = "<C-L>"; action = ":bn<CR>"; }
        {
          key = "<C-K>";
          action = "kkkkkkkkkkkkkkkkkkkkk";
        }
        {
          key = "<C-J>";
          action = "jjjjjjjjjjjjjjjjjjjjj";
        }

        # \s to replace globally the word under the cursor
        {
          key = "<Leader>s";
          action = ":%s/\\<<C-r><C-w>\\>/";
        }

        # PLUGINS

        # undotree
        {
          key = "<Space>u";
          action = "<Cmd>UndotreeToggle<CR>";
          options = {
            silent = true;
          };
        }

      ];
    };
  };

  imports = [
    nixvim.homeManagerModules.nixvim
    ./code.nix
    ./completion.nix
    ./decoration.nix
    ./git.nix
    ./lsp.nix
  ];
}
