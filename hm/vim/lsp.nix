{
  pkgs,
  ...
}:
{
  config = {
    programs.nixvim = {
      extraConfigLua = ''
        require'lsp_signature'.on_attach({
            hint_enable = false,
        })
      '';
      extraPlugins = with pkgs.vimPlugins; [
        # Language server
        lsp_signature-nvim # Show argument definition when typing a function
      ];
      keymaps = [
        {
          mode = "n";
          key = "<space>wl";
          action.__raw = ''
            function()
              print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
            end
          '';
          options.silent = true;
        }
        {
          mode = "n";
          key = "<space>f";
          action.__raw = ''
            function()
              vim.lsp.buf.format { async = true }
            end
          '';
          options.silent = true;
        }
      ];
      plugins = {
        # Language Server
        lsp = {
          enable = true;
          keymaps = {
            silent = true;
            diagnostic = {
              "<Space>e" = "open_float";
              "[e" = "goto_prev";
              "]e" = "goto_next";
            };
            lspBuf = {
              "gD" = "declaration";
              "K" = "hover";
              "gi" = "implementation";
              "<C-S-k>" = "signature_help";
              "<space>wa" = "add_workspace_folder";
              "<space>wr" = "remove_workspace_folder";
              "<space>D" = "type_definition";
              "<space>rn" = "rename";
              "<space>ca" = "code_action"; # Reference has a binding for visual mode, but it doesn't work
            };
          };
        };
        telescope.keymaps = {
          gr = "lsp_references";
          ge = "diagnostics";
          gd = "lsp_definitions";
          gs = "lsp_document_symbols";
        };
        nvim-lightbulb = {
          # Shows a lightbulb whenever a codeAction is available under the cursor
          enable = true;
          settings.autocmd.enabled = true;
        };
      };
    };
  };
}
