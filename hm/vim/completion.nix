{
  pkgs,
  ...
}:
{
  config = {
    # Completion
    programs.nixvim = {
      extraConfigLuaPre = builtins.readFile ./completion_pre.lua;
      plugins = {
        # Snippets
        # UltiSnips should be the better snippet engine, but:
        # - Not built-in into Nixvim yet (UPST?)
        # - Couldn't manage to make it work with friendly-snippets, which seems to give better snippets for Python at least
        # - Tab switching between fields seems hard to configure
        luasnip = {
          enable = true;
          fromVscode = [
            { paths = "${pkgs.vimPlugins.friendly-snippets}"; }
            # { paths = "${pkgs.vimPlugins.vim-snippets}"; } # UPST Needs snipmate support
          ];
        };

        # Completion
        cmp = {
          enable = true;
          settings = {
            mapping = {
              # Proposed example, since there's no default
              "<C-Space>" = "cmp.mapping.complete()";
              "<C-d>" = "cmp.mapping.scroll_docs(-4)";
              "<C-e>" = "cmp.mapping.close()";
              "<C-f>" = "cmp.mapping.scroll_docs(4)";
              "<CR>" = "cmp.mapping.confirm({ select = true })";
              "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
              "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
            };
            sources = [
              # Respective plugins will get installed automatically
              { name = "buffer"; }
              { name = "calc"; }
              { name = "nvim_lsp"; }
              { name = "path"; }
              { name = "luasnip"; }
            ];
            snippet.expand = "function(args) require('luasnip').lsp_expand(args.body) end";
          };
        };
        lspkind.enable = true; # Add icons to LSP completions
      };
    };
  };
}
