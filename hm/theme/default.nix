{
  pkgs,
  config,
  stylix,
  ...
}:
{
  config = {
    stylix = {
      enable = true;
      polarity = config.frogeye.polarity;
      fonts = {
        monospace = {
          package = pkgs.nerdfonts.override {
            fonts = [ "DejaVuSansMono" ]; # Choose from https://github.com/NixOS/nixpkgs/blob/6ba3207643fd27ffa25a172911e3d6825814d155/pkgs/data/fonts/nerdfonts/shas.nix
          };
          name = "DejaVuSansM Nerd Font";
        };
      };
    };
  };
  imports = [
    stylix.homeManagerModules.stylix
  ];
}
