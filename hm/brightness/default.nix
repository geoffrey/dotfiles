# Light theme during the day, dark theme during the night (not automatic)
{
  pkgs,
  lib,
  config,
  ...
}:
let
  phases = [
    {
      command = "jour";
      specialisation = null;
    }
    {
      command = "crepuscule";
      specialisation = "dark";
    }
    {
      command = "nuit";
      specialisation = "dark";
    }
  ];
  mod = config.xsession.windowManager.i3.config.modifier;
in
{
  config = {
    home.packages =
      (map (
        phase:
        (pkgs.writeShellScriptBin phase.command ''
          switch="/nix/var/nix/profiles/system${
            lib.strings.optionalString (phase.specialisation != null) "/specialisation/${phase.specialisation}"
          }/bin/switch-to-configuration"
          if [ -x "$switch" ]
          then
              sudo "$switch" test &
              sudo "$switch" boot &
          fi
          ${builtins.getAttr phase.command config.frogeye.desktop.phasesCommands}
          wait
        '')
      ) phases)
      ++ (with pkgs; [
        brightnessctl
      ]);
    xsession.windowManager.i3.config.keybindings = {
      XF86MonBrightnessUp = "exec ${pkgs.brightnessctl}/bin/brightnessctl set +5%";
      XF86MonBrightnessDown = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 5%-";
      "${mod}+F6" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set 1%-";
      "${mod}+F7" = "exec ${pkgs.brightnessctl}/bin/brightnessctl set +1%";
    };
  };
}
