# Untested post-nix
{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev.go {
    frogeye = {
      direnv = {
        GOPATH = "${config.xdg.cacheHome}/go";
      };
    };
    home = {
      packages = with pkgs; [
        go
      ];
      sessionPath = [
        "${config.home.sessionVariables.GOPATH}"
      ];
    };
  };
}
