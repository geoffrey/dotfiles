# Prose is a programming language, fight me
{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev.prose {
    home = {
      packages = with pkgs; [
        hunspell
        hunspellDicts.en_GB-ize
        hunspellDicts.en_US
        hunspellDicts.fr-moderne
        hunspellDicts.nl_NL
        # TODO libreoffice-extension-languagetool or libreoffice-extension-grammalecte-fr
      ];
    };
    programs.nixvim = {
      autoCmd = [
        # vim-easy-align: Align Markdown tables with |
        {
          event = "FileType";
          pattern = "markdown";
          command = "vmap <Bar> :EasyAlign*<Bar><Enter>";
        }
      ];
      extraPlugins =
        with pkgs.vimPlugins;
        lib.optionals config.programs.pandoc.enable [
          vim-pandoc # Pandoc-specific stuff because there's no LSP for it
          vim-pandoc-syntax
        ];
      extraConfigVim = lib.optionalString config.programs.pandoc.enable ''
        let g:pandoc#modules#disabled = ["folding"]
        let g:pandoc#spell#enabled = 0
        let g:pandoc#syntax#conceal#use = 0
      '';
      plugins.none-ls = {
        enable = true;
        sources = {
          # LanguageTool
          diagnostics.ltrs.enable = true;
        };
      };
    };
  };
}
