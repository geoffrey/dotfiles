{
  ...
}:
{
  imports = [
    ./3d.nix
    ./c.nix
    ./common.nix
    ./go.nix
    ./node.nix
    ./prose.nix
    ./python.nix
  ];
}
