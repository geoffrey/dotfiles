# Untested post-nix
{
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev.node {
    frogeye = {
      direnv = {
        npm_config_cache = "${config.xdg.cacheHome}/npm";
        YARN_CACHE_FOLDER = "${config.xdg.cacheHome}/yarn";
      };
    };
    home = {
      sessionVariables = {
        NODE_REPL_HISTORY = "${config.xdg.cacheHome}/node_repl_history";
        YARN_DISABLE_SELF_UPDATE_CHECK = "true"; # This also disable the creation of a ~/.yarnrc file
      };
    };
    programs.bash.shellAliases = {
      bower = "bower --config.storage.packages=${config.xdg.cacheHome}/bower/packages --config.storage.registry=${config.xdg.cacheHome}/bower/registry --config.storage.links=${config.xdg.cacheHome}/bower/links";
    };
  };
}
