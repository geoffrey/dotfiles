{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev.c {
    frogeye = {
      direnv = {
        CCACHE_DIR = "${config.xdg.cacheHome}/ccache"; # The config file alone seems to be not enough
      };
      junkhome = [
        "binwalk" # Should use .config according to the GitHub code though
        "cmake"
        "ddd"
        "ghidra"
      ];
    };
    home = {
      packages = with pkgs; [
        binwalk
        ccache
        clang
        cmake
        ddd
        gdb
        gnumake
        valgrind
      ];
      sessionVariables = {
        CCACHE_CONFIGPATH = "${config.xdg.configHome}/ccache.conf";
      };
    };
    programs.bash.shellAliases = {
      gdb = "gdb -x ${config.xdg.configHome}/gdbinit";
    };
    programs.nixvim.plugins = {
      dap.enable = true; # Debug Adapter Protocol client
      lsp.servers.clangd.enable = true;
    };
    xdg.configFile = {
      "ccache.conf" = {
        text = "ccache_dir = ${config.xdg.cacheHome}/ccache";
      };
      gdbinit = {
        text = ''
          define hook-quit
              set confirm off
          end
        '';
      };
    };
  };
}
