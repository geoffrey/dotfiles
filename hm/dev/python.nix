{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev.python {
    home = {
      packages = with pkgs; [
        python3
        python3Packages.ipython
      ];
      sessionVariables = {
        PYTHONSTARTUP = "${config.xdg.configHome}/pythonstartup.py";
      };
    };
    programs.bash.shellAliases = {
      ipython = "ipython --no-confirm-exit --pdb";
    };
    programs.nixvim.plugins.lsp.servers.pylsp = {
      # Python
      enable = config.frogeye.dev.python;
      settings.plugins = {
        black.enabled = true;
        flake8 = {
          enabled = true;
          maxLineLength = 88; # Compatibility with Black
        };
        isort.enabled = true;
        mccabe.enabled = true;
        pycodestyle = {
          enabled = true;
          maxLineLength = 88; # Compatibility with Black
        };
        pyflakes.enabled = true;
        pylint.enabled = true;
        pylsp_mypy = {
          enabled = true;
          overrides = [
            "--cache-dir=${config.xdg.cacheHome}/mypy"
            "--ignore-missing-imports"
            "--disallow-untyped-defs"
            "--disallow-untyped-calls"
            "--disallow-incomplete-defs"
            "--disallow-untyped-decorators"
            true
          ];
        };
        # TODO Could add some, could also remove some
      };
    };
    xdg.configFile = {
      "pythonstartup.py" = {
        text = (builtins.readFile ./pythonstartup.py);
      };
    };
  };
}
