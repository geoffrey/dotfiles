{ pkgs, config, ... }:
{
  # TODO Maybe should be per-directory dotenv
  # Or not, for neovim
  config = {
    # Always on
    home.packages =
      with pkgs;
      [
        # Common
        perf-tools
        jq
        yq
        universal-ctags
        cloc

        # Network
        socat
        dig
        whois
        nmap
        tcpdump
        mtr
        traceroute

        # nix
        lix
        nixfmt-rfc-style

        # Always on (graphical)
      ]
      ++ lib.optionals config.frogeye.desktop.xorg [
        # Common
        # zeal-qt6 # Offline documentation
        sqlitebrowser

        # Network
        wireshark-qt

        # Docker
      ]
      ++ lib.optionals config.frogeye.dev.docker [
        docker
        docker-compose

        # VM (graphical)
      ]
      ++ lib.optionals (config.frogeye.desktop.xorg && config.frogeye.dev.vm) [
        virt-manager
      ];

    programs.nixvim.plugins.lsp.servers = {
      ansiblels.enable = config.frogeye.dev.ansible; # Ansible
      bashls.enable = true; # Bash
      jsonls.enable = true; # JSON
      lua_ls.enable = true; # Lua (for Neovim debugging)
      perlpls.enable = config.frogeye.dev.perl; # Perl
      phpactor.enable = config.frogeye.dev.php; # PHP
      # Nix
      nil_ls = {
        enable = true;
        settings = {
          formatting.command = [ "nixfmt" ];
          nix.flake = {
            autoArchive = true;
            autoEvalInputs = true;
          };
        };
      };
      # TODO Something for SQL. sqls is deprecated, sqlls is not in Nixpkgs. Probably needs a DB connection configured anyways?
      yamlls.enable = true; # YAML
      # TODO Check out none-ls
    };
  };
}
