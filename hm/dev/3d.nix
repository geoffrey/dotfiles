{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.dev."3d" {
    home = {
      packages = with pkgs; [
        blender
        openscad-unstable # 2022+ parses files much faster
        orca-slicer
      ];
    };
    programs.nixvim.plugins = {
      # openscad.enable = true; # Doesn't do anything besides annoying popups
      lsp.servers.openscad_lsp.enable = true;
    };
    xdg.dataFile = {
      "OpenSCAD/libraries/BOSL2".source = pkgs.fetchFromGitHub {
        owner = "BelfrySCAD";
        repo = "BOSL2";
        rev = "ff7e8b8611022b1ce58ea2a1e076028d3d7d40ff"; # no tags, no release
        hash = "sha256-esKgXyKLudDfWT2pxOjltZsQ9N6Whlf4zhxd071COzQ=";
      };
      "OpenSCAD/libraries/MCAD".source = pkgs.fetchFromGitHub {
        owner = "openscad";
        repo = "MCAD";
        rev = "bd0a7ba3f042bfbced5ca1894b236cea08904e26"; # no tags, no release
        hash = "sha256-rnrapCe5BkdibbCYVyGZi0l1/8DZxoDnulK37fwZbqo=";
      };
    };
  };
}
