{
  lib,
  config,
  ...
}:
# Does not implement everything .netrc allows
# (starting with: changing the .netrc position)
# but neither do clients anyways.
let
  cfg = config.frogeye.netrc;
in
{
  config = {
    frogeye.passwordFiles = [
      {
        path = "${config.home.homeDirectory}/.netrc";
        text = lib.trivial.pipe cfg [
          builtins.attrValues
          (builtins.map (
            n: "machine ${n.machine} login @${n.machine}_login@ password @${n.machine}_password@"
          ))
          lib.strings.concatLines
        ];
        passwords = lib.trivial.pipe cfg [
          builtins.attrValues
          (builtins.map (n: [
            {
              name = "@${n.machine}_login@";
              value = {
                inherit (n) path;
                selector = n.login;
              };
            }
            {
              name = "@${n.machine}_password@";
              value = {
                inherit (n) path;
                selector = n.password;
              };
            }
          ]))
          lib.lists.flatten
          builtins.listToAttrs
        ];
      }
    ];
  };
  options = {
    frogeye.netrc = lib.mkOption {
      default = { };
      description = "Entries to add to .netrc";
      type = lib.types.attrsOf (
        lib.types.submodule (
          { config, name, ... }:
          {
            options = {
              machine = lib.mkOption {
                type = lib.types.str;
                default = name;
                readOnly = true;
                internal = true;
              };
              login = lib.mkOption {
                type = lib.types.nullOr lib.types.str;
                default = "email";
                description = "Password selector that will be used as the login field";
              };
              password = lib.mkOption {
                type = lib.types.nullOr lib.types.str;
                default = null;
                description = "Password selector that will be used as the password field";
              };
              path = lib.mkOption {
                type = lib.types.str;
                description = "Path to the password store entry";
              };
            };
          }
        )
      );
    };
  };
}
