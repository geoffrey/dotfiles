{
  pkgs,
  config,
  ...
}:
{
  config = {
    home.packages = with pkgs; [
      htop
      iftop
      iotop
      lsof
      progress
      pv
      speedtest-cli
      strace
    ];
    programs.bash.shellAliases = {
      iftop = "iftop -c ${config.xdg.configHome}/iftoprc";
      tracefiles = ''${pkgs.strace}/bin/strace -f -t -e trace=file'';
    };
    xdg = {
      configFile = {
        "iftoprc" = {
          text = ''
            port-resolution: no
            promiscuous: no
            port-display: on
            link-local: yes
            use-bytes: yes
            show-totals: yes
            log-scale: yes
          '';
        };
      };
    };
  };

}
