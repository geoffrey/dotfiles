{
  pkgs,
  lib,
  ...
}:
{
  config = {
    home.packages = with pkgs; [
      nvd
      nix-diff
      nix-tree
      nix-output-monitor
    ];
    programs.nix-index = {
      # For non-NixOS systems
      enable = false; # TODO Index is impossible to generate, should use https://github.com/nix-community/nix-index-database
      # but got no luck without flakes
      enableZshIntegration = true;
    };
    nix = {
      package = lib.mkDefault pkgs.lix;
      settings = {
        experimental-features = [
          "nix-command"
          "flakes"
        ];
        warn-dirty = false;
      };
    };
  };
}
