{ lib, config, ... }:
{
  config = {
    frogeye = {
      # TODO Move to relevant config file. Rest can probably removed.
      direnv = {
        CARGOHOME = "${config.xdg.cacheHome}/cargo"; # There are config in there that we can version if one want
        DASHT_DOCSETS_DIR = "${config.xdg.cacheHome}/dash_docsets";
        GRADLE_USER_HOME = "${config.xdg.cacheHome}/gradle";
        MIX_ARCHIVES = "${config.xdg.cacheHome}/mix/archives";
        MONO_GAC_PREFIX = "${config.xdg.cacheHome}/mono";
        PARALLEL_HOME = "${config.xdg.cacheHome}/parallel";
        TERMINFO = "${config.xdg.configHome}/terminfo";
        WINEPREFIX = "${config.xdg.stateHome}/wineprefix/default";
      };
      junkhome = [
        "adb"
        "audacity"
        "cabal" # TODO May have options but last time I tried it it crashed
        "itch"
        "simplescreenrecorder" # Easy fix https://github.com/MaartenBaert/ssr/blob/1556ae456e833992fb6d39d40f7c7d7c337a4160/src/Main.cpp#L252
        "vd"
        "wpa_cli"
        # TODO Maybe we can do something about node-gyp
      ];
    };
    home = {
      activation.createDirenvFolders = lib.hm.dag.entryAfter [ "writeBoundary" ] (
        lib.strings.concatLines (
          map (d: "mkdir -p ${d}") (
            (builtins.attrValues config.frogeye.direnv) ++ [ "${config.xdg.cacheHome}/junkhome" ]
          )
        )
      );

      sessionVariables = config.frogeye.direnv;
    };
    programs.bash.shellAliases = lib.attrsets.mergeAttrsList (
      map (p: { "${p}" = "HOME=${config.xdg.cacheHome}/junkhome ${p}"; }) config.frogeye.junkhome
    );
  };
  options.frogeye = {
    direnv = lib.mkOption {
      default = { };
      example = lib.literalExpression ''
        {
          DASHT_DOCSETS_DIR = "''${config.xdg.cacheHome}/dash_docsets";
        }
      '';
      description = ''
        Environment variables for which the value is a folder that will be automatically created.
        Useful for keeping programs data out of $HOME for programs that won't create the directory themselves.
      '';
      type = lib.types.attrsOf lib.types.str;
    };
    junkhome = lib.mkOption {
      default = [ ];
      description = ''
        Program names that will be run with a different HOME so they don't clutter the real one.
        Useful for programs that don't follow the XDG specification and tend to advertise themselves.
      '';
      type = lib.types.listOf lib.types.str;
    };
    # TODO Should make a nix package wrapper instead, so it also works from dmenu
  };
}
