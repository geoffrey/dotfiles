{ config, ... }:
{
  config = {
    home = {
      sessionVariables = {
        LESSHISTFILE = "${config.xdg.stateHome}/lesshst";
        LESS = "-R";
        LESS_TERMCAP_mb = "$(echo $'\\E[1;31m')"; # begin blink
        LESS_TERMCAP_md = "$(echo $'\\E[1;36m')"; # begin bold
        LESS_TERMCAP_me = "$(echo $'\\E[0m')"; # reset bold/blink
        LESS_TERMCAP_se = "$(echo $'\\E[0m')"; # reset reverse video
        LESS_TERMCAP_so = "$(echo $'\\E[01;44;33m')"; # begin reverse video
        LESS_TERMCAP_ue = "$(echo $'\\E[0m')"; # reset underline
        LESS_TERMCAP_us = "$(echo $'\\E[1;32m')"; # begin underline
      };
    };
  };
}
