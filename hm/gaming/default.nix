{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.gaming {
    # Using config.nixpkgs.<something> creates an infinite recursion,
    # but the above might not be correct in case of cross-compiling?
    home = {
      packages = with pkgs; [
        # gaming
        dolphin-emu
        ryujinx
        prismlauncher
        # TODO factorio

        steam # Common pitfall: https://github.com/NixOS/nixpkgs/issues/86506#issuecomment-623746883
        # itch # butler-15.21.0 is broken
        (pkgs.python3Packages.ds4drv.overrideAttrs (old: {
          src = fetchFromGitHub {
            owner = "TheDrHax";
            repo = "ds4drv-cemuhook";
            rev = "a58f63b70f8d8efa33e5e82a8888a1e08754aeed";
            sha256 = "sha256-oMvHw5zeO0skoiqLU+EdjUabTvkipeBh+m8RHJcWZP8=";
          };
        }))
      ];
      sessionVariables = {
        BOOT9_PATH = "${config.xdg.dataHome}/citra-emu/sysdata/boot9.bin";
      };
    };
  };
}
