{ ... }:
{
  imports = [
    ../common/frogarized
    ../options.nix
    ./accounts
    ./brightness
    ./common.nix
    ./desktop
    ./dev
    ./extra
    ./gaming
    ./git
    ./gpg
    ./homealone.nix
    ./monitoring
    ./nix
    ./pager
    ./password
    ./prompt
    ./rebuild
    ./shell
    ./ssh.nix
    ./theme
    ./tmux
    ./vim
  ];
}
