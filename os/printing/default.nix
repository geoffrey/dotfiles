{ lib, config, ... }:
{
  config = lib.mkIf config.services.printing.enable {
    hardware.sane.enable = true;
    services.avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
    users.users.geoffrey.extraGroups = [ "lp" "scanner" ];
  };
}
