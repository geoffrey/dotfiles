{ disko, ... }:
{
  imports = [
    ../options.nix
    ../common/frogarized
    ./battery.nix
    ./boot
    ./ccc
    ./common.nix
    ./cuda
    ./desktop
    ./dev
    disko.nixosModules.disko
    ./dns
    ./gaming
    ./geoffrey.nix
    ./password
    ./printing
    ./remote-builds
    ./style
    ./syncthing
    ./time
    ./wireless
  ];
}
