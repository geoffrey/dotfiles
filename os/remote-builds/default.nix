{
  pkgs,
  lib,
  config,
  ...
}:
let
  vivariumBuilderDefault = {
    systems = [
      "x86_64-linux"
      "aarch64-linux"
    ];
    protocol = "ssh-ng";
    sshUser = "nixremote";
    # sshKey doesn't work
  };
  # MANU ssh-keygen -y -f /etc/ssh/ssh_host_ed25519_key | base64 -w0
  vivariumBuilders = [
    {
      hostName = "ludwig.clowncar.frogeye.fr";
      publicHostKey = "c3NoLWVkMjU1MTkgQUFBQUMzTnphQzFsWkRJMU5URTVBQUFBSU41SXZhMzNXeGplN095cHVEUHBSakFNMTlvRUtEVDRiYlpUTm82V1FLZTAgZ2VvZmZyZXlAY3VyYWNhbwo=";
      maxJobs = 4;
    }
    {
      hostName = "abavorana.frogeye.fr";
      publicHostKey = "c3NoLWVkMjU1MTkgQUFBQUMzTnphQzFsWkRJMU5URTVBQUFBSU5iNzcrS01tRHI0MVhZdmZITXQvK3NHMkJCSEIzYUl4M045WDNVejhFaUogZ2VvZmZyZXlAY3VyYWNhbwo=";
      supportedFeatures = [
        "nixos-test"
        "benchmark"
        "big-parallel"
        "kvm"
      ];
      maxJobs = 8;
    }
  ];
  # MANU pass vivarium/lemmy/remote-builds/cache | nix key convert-secret-to-public | cat
  publicKeys = [
    "ludwig.clowncar.frogeye.fr:jTlN0fCOLU49M3LQw5j/u++Gmwrsv3m9RGs0slSg6r0="
    "abavorana.frogeye.fr:rcKZ9gwaIQLcst/vbhbF7meUQD5sveT2QQN4a+Zo1BM="
  ];
in
{
  config = {
    system.activationScripts.remote = {
      supportsDryActivation = true;
      text = ''
        mkdir -p /root/.ssh
        cat ${
          pkgs.writeText "root-ssh-config" (
            lib.strings.concatLines (
              builtins.map (builder: ''
                Host ${builder.hostName}
                  VerifyHostKeyDNS yes
                  UpdateHostKeys yes
              '') vivariumBuilders
              # Check SSHFP records so hopefully no need to manually add SSH host keys,
              # yet add them to known_hosts should we not be using DNSSEC-validating DNS.
              # No persistent socket since sometimes it goes a weird state and have to remove them.
            )
          )
        } > /root/.ssh/config
      '';
    };
    nix = {
      buildMachines = builtins.map (
        vivariumBuilder: vivariumBuilderDefault // vivariumBuilder
      ) vivariumBuilders;
      distributedBuilds = true;
      settings = {
        builders-use-substitutes = true;
        trusted-public-keys = publicKeys;
        substituters = builtins.map (
          builder: "${builder.protocol}://${builder.sshUser}@${builder.hostName}"
        ) config.nix.buildMachines;
      };
    };
  };
}
