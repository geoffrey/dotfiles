{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkMerge [
    (lib.mkIf config.frogeye.dev.docker {
      virtualisation.docker.enable = true;
      users.users.geoffrey.extraGroups = [ "docker" ];
    })
    (lib.mkIf config.frogeye.dev.vm {
      # Required to mount filesystems. Also need to add
      # <binary path="/run/current-system/sw/bin/virtiofsd"/>
      # to the VM's filesystem XML config
      environment.systemPackages = [ pkgs.virtiofsd ];
      virtualisation = {
        libvirtd.enable = true;
        spiceUSBRedirection.enable = true;
      };
      users.extraGroups.libvirtd.members = [ "geoffrey" ];
    })
    {
      users.extraGroups.dialout.members = [ "geoffrey" ];
      # Always comes in handy, not enabled by default anyways
      programs.nix-ld.enable = true;
    }
  ];
}
