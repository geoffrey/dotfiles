{
  pkgs,
  config,
  ...
}:
{
  networking = {
    hostName = config.frogeye.name;
    domain = "geoffrey.frogeye.fr";
  };

  time.timeZone = "Europe/Amsterdam";

  boot.tmp.cleanOnBoot = true;

  # TODO qwerty-fr for console

  # Enable CUPS to print documents
  services.printing.enable = true;

  # Enable passwordless sudo
  # TODO execWheelOnly? sudo-rs?
  security.sudo.extraRules = [
    {
      groups = [ "wheel" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ];
        }
      ];
    }
  ];

  # UPST disko --root-mountpoint doesn't work when using flake, workaround:
  disko.rootMountPoint = "/mnt/nixos";

  environment.systemPackages = with pkgs; [
    wget
    kexec-tools
    neovim # So we have a working editor in rescue mode
    git # Needed for all the fetchFromGit in this repo on nixos-rebuild
  ];

  i18n = {
    defaultLocale = "nl_NL.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "en_DK.UTF-8"; # Use YYYY-MM-DD, Thunderbird & qutebrowser use own settings
    };
  };

  nix = {
    package = pkgs.lix;
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
      warn-dirty = false;
    };
  };

  programs = {
    adb.enable = true;
    # Enable compilation cache
    ccache.enable = true;
    # TODO Not enough, see https://nixos.wiki/wiki/CCache.
    # Might want to see if it's worth using on NixOS
    less.lessopen = null; # Don't use lessopen

    # Let users mount disks
    udevil.enable = true;
  };

  services = {
    # More aggressive OoM killer so we never hang
    earlyoom.enable = true;

    # Enable the OpenSSH daemon
    openssh.enable = true;

    # Prevent power button from shutting down the computer.
    # On Pinebook it's too easy to hit,
    # on others I sometimes turn it off when unsuspending.
    logind.extraConfig = "HandlePowerKey=ignore";

    udev.extraRules = ''
      # DYMO LabelPOINT 350
      ACTION=="add", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0922", ATTRS{idProduct}=="0015", MODE="0666"
    '';

  };

  # TODO Hibernation?

  # Use defaults from
  system.stateVersion = "24.11";

}
