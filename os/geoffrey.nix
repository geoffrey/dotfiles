{
  pkgs,
  lib,
  config,
  home-manager,
  ...
}:
{
  config = {
    users.users.root.initialHashedPassword = "$y$j9T$e64bjL7iyVlniEKwKbM9g0$cCn74za0r6L9QMO20Fdxz3/SX0yvhz3Xd6.2BhtbRL1"; # Not a real password

    users.users.geoffrey = {
      isNormalUser = true;
      extraGroups = [
        "adbusers"
        "wheel"
      ];
      shell = pkgs.zsh;

      initialHashedPassword = "$y$j9T$e64bjL7iyVlniEKwKbM9g0$cCn74za0r6L9QMO20Fdxz3/SX0yvhz3Xd6.2BhtbRL1"; # Not a real password
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPE41gxrO8oZ5n3saapSwZDViOQphm6RzqgsBUyA88pU geoffrey@frogeye.fr"
      ];
    };

    # Won't allow to set the shell otherwise,
    # even though home-manager sets it
    programs.zsh.enable = true;

    nix.settings.trusted-users = [ "geoffrey" ];

    home-manager = {
      users.geoffrey =
        { pkgs, ... }:
        {
          frogeye = lib.mkDefault config.frogeye;
          # Propagating options that way doesn't seem to conserve priority info,
          # this is not great. Hopefully mkDefault resolve conflicts.
        };
      # Makes VMs able to re-run
      useUserPackages = true;
      # Adds consistency
      useGlobalPkgs = true;
    };

    specialisation = {
      dark.configuration.frogeye.polarity = "dark";
    };

    # Fix https://nix-community.github.io/home-manager/index.html#_why_do_i_get_an_error_message_about_literal_ca_desrt_dconf_literal_or_literal_dconf_service_literal
    programs.dconf.enable = true;

    # Because everything is encrypted and I'm the only user, this is fine.
    services.displayManager.autoLogin.user = "geoffrey";
  };
  imports = [
    home-manager.nixosModules.home-manager
  ];
}
