{
  config,
  ...
}:
{
  boot.loader = {
    grub = {
      enable = true;
      efiSupport = true;
      efiInstallAsRemovable = !config.boot.loader.efi.canTouchEfiVariables;
      device = "nodev"; # Don't install on MBR
    };
  };

}
