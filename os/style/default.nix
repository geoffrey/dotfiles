{
  pkgs,
  config,
  stylix,
  ...
}:
{
  config = {
    boot = {
      plymouth.enable = true;
      initrd.systemd.enable = true;
    };
    stylix = {
      enable = true;
      homeManagerIntegration.autoImport = false; # Makes config reuse easier
      polarity = "dark";
      targets.plymouth.logo =
        pkgs.runCommand "flower.png" { }
          "${pkgs.inkscape}/bin/inkscape ${
            pkgs.substituteAll {
              src = ./flower.svg;
              color = config.lib.stylix.colors.withHashtag.base07;
            }
          } -w 256 -o $out";
      # UPST Default grub font is sansSerif, which doesn't work.
      # Maybe because people patch mono with nerdfonts and that isn't compatible?
    };
  };
  imports = [
    stylix.nixosModules.stylix
  ];
}
