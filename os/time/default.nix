{
  lib,
  config,
  ...
}:
{
  config = {
    # Apparently better than reference implementation
    services.chrony.enable = true;

    networking = {
      # Using community provided service
      timeServers = map (n: "${toString n}.europe.pool.ntp.org") (lib.lists.range 0 3);

      # Only try to sync time when we have internet connection
      dhcpcd.runHook = ''
        if $if_up
        then
            /run/wrappers/bin/sudo ${config.services.chrony.package}/bin/chronyc online
        elif $if_down
        then
            /run/wrappers/bin/sudo ${config.services.chrony.package}/bin/chronyc offline
        fi
      '';
    };

    # Allow dhcpcd to control chrony
    security.sudo.extraRules = [
      {
        users = [ "dhcpcd" ];
        commands =
          builtins.map
            (arg: {
              command = "${config.services.chrony.package}/bin/chronyc ${arg}";
              options = [ "NOPASSWD" ];

            })
            [
              "online"
              "offline"
            ];
      }
    ];
    systemd.services.dhcpcd.serviceConfig.NoNewPrivileges = false;

  };
}
