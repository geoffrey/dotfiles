{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.gaming {
    programs.steam.enable = true;
    hardware.graphics.enable32Bit = true; # Needed by Steam
    services = {
      udev.packages = [ pkgs.python3Packages.ds4drv ];
      xserver.config = ''
        # Disable mouse support for joypad
        Section "InputClass"
                Identifier "joystick catchall"
                MatchIsJoystick "on"
                MatchDevicePath "/dev/input/event*"
                Driver "joystick"
                Option "StartKeysEnabled" "False"
                Option "StartMouseEnabled" "False"
        EndSection
        # Same thing for DualShock 4 touchpad
        Section "InputClass"
               Identifier   "ds4-touchpad"
               Driver       "libinput"
               MatchProduct "Wireless Controller Touchpad"
               Option       "Ignore" "True"
        EndSection
      '';
    };
  };
}
