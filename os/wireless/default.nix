{
  pkgs,
  lib,
  ...
}:
let
  importScript = pkgs.writers.writePython3 "install-wifi-import" {
    libraries = [ pkgs.python3Packages.pyaml ];
  } (builtins.readFile ./import.py);
  applyScript = pkgs.writers.writePython3 "install-wifi-apply" { } (builtins.readFile ./apply.py);
in
{
  environment.systemPackages = [
    (pkgs.writeShellApplication {
      name = "install-wifi";
      runtimeInputs = with pkgs; [
        wpa_supplicant
        diffutils
      ];
      text = ''
        temp="$(mktemp --directory --suffix="-install-wifi")"
        cd "$temp"

        # Save config for diffing later
        wpa_cli save_config > /dev/null
        cat <(sudo cat /run/wpa_supplicant/wpa_supplicant.conf) > old.conf

        # Export Wi-Fi config from pass
        ${importScript}

        # Save on persistent storage for boot
        sudo chown root:root wireless_networks.json
        sudo chmod "u=r" wireless_networks.json
        sudo mkdir -p /etc/keys
        sudo mv -f wireless_networks.json /etc/keys

        # Apply configuration
        sudo ${applyScript}

        # Diff the config
        wpa_cli save_config > /dev/null
        cat <(sudo cat /run/wpa_supplicant/wpa_supplicant.conf) > new.conf
        diff --color=auto -U 5 old.conf new.conf

        rm old.conf new.conf
        cd /
        rmdir "$temp"
      '';
      # This relies on pass password store with wifi/${name} entries,
      # containing wpa_supplicant networks loosely converted to YAML
      # (see import.py script)
    })
  ];
  # wireless support via wpa_supplicant
  networking = {
    wireless = {
      enable = true;
      extraConfig = ''
        country=NL
      '';
      # Public wireless networks
      networks = lib.genAttrs [
        "CDWiFi"
        "EurostarTrainsWiFi"
        "_SNCF gare-gratuit"
        "_SNCF_WIFI_INOUI"
        "Wifi in de trein"
        "WiFi in de trein"
        "_WIFI_LYRIA"
        "WIFIonICE"
      ] (ssid: { });
      userControlled.enable = true; # Allow some control with wpa_cli
    };
  };

  # Wait until there's a connection to fetch time
  services.chrony.serverOption = "offline";

  systemd.services.wifi_apply = {
    after = [ "wpa_supplicant.service" ];
    wantedBy = [ "wpa_supplicant.service" ];
    path = with pkgs; [ wpa_supplicant ];
    script = ''
      for i in {1..50}; do wpa_cli status &> /dev/null && break; sleep 0.1; done
      ${applyScript}
    '';
  };
}
