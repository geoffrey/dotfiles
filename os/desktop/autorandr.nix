{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf (builtins.length config.frogeye.desktop.x11_screens > 1) {
    services = {
      autorandr.enable = true;
      xserver.displayManager.lightdm.extraConfig =
        let
          setupScript = "${
            pkgs.writeShellApplication {
              name = "greeter-setup-script";
              runtimeInputs = [ pkgs.autorandr ];
              text = ''
                autorandr --change
              '';
            }
          }/bin/greeter-setup-script";
        in
        ''
          [Seat:*]
          display-setup-script = ${setupScript}
        '';
    };
  };
}
