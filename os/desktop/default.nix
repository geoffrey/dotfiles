{
  pkgs,
  lib,
  config,
  ...
}:
{
  config = lib.mkIf config.frogeye.desktop.xorg {
    boot.kernelModules = [ "i2c-dev" ]; # Allows using ddcutil
    security.rtkit.enable = true; # Recommended for pipewire
    services = {
      blueman.enable = true;
      displayManager.defaultSession = "none+i3";
      udev.packages = with pkgs; [ ddcutil ]; # TODO Doesn't seem to help
      pipewire = {
        enable = true;
        alsa = {
          enable = true;
          support32Bit = true;
        };
        pulse.enable = true; # Required for a lot of apps, probably
        wireplumber.extraConfig = {
          # Using mains-powered speakers
          # https://wiki.archlinux.org/title/PipeWire#Noticeable_audio_delay_or_audible_pop/crack_when_starting_playback
          preventGetTogetherDuoFromShuttingDown."monitor.bluez.rules" = [
            {
              matches = [ { "node.name" = "~bluez_output.41_42_D2_B5_13_72.*"; } ];
              actions.update-props = {
                "session.suspend-timeout-seconds" = 0;
                "dither.method" = "wannamaker3";
                "dither.noise" = 4;
              };
            }
          ];
        };
      };
      # Video
      xserver = {
        enable = true;
        windowManager.i3.enable = true;

        # Keyboard layout
        xkb = {
          extraLayouts.qwerty-fr = {
            description = "QWERTY-fr";
            languages = [ "fr" ];
            symbolsFile = "${
              pkgs.stdenv.mkDerivation {
                name = "qwerty-fr-keypad";
                src = pkgs.fetchFromGitHub {
                  owner = "qwerty-fr";
                  repo = "qwerty-fr";
                  rev = "3a4d13089e8ef016aa20baf6b2bf3ea53de674b8";
                  sha256 = "sha256-wn5n6jJVDrQWJze8xYF2nEY8a7mHI3hVO4xsT4LMo9c=";
                };
                patches = [ ./qwerty-fr-keypad.diff ];
                # TODO This doesn't seem to be applied... it's the whole point of the derivation :(
                installPhase = ''
                  runHook preInstall
                  mkdir -p $out/linux
                  cp $src/linux/us_qwerty-fr $out/linux
                  runHook postInstall
                '';
              }
            }/linux/us_qwerty-fr";
          };
          layout = "qwerty-fr";
        };
      };
      # Disco
    };

    # Enable sound & bluetooth
    hardware.bluetooth.enable = true;

    # So we can use gnome3 pinentry flavour
    services.dbus.packages = [ pkgs.gcr ];
  };
  imports = [
    ./autorandr.nix
  ];
}
