# Need nvidia proprietary drivers to work
{
  nixpkgs,
  config,
  lib,
  ...
}:
{
  config = lib.mkIf config.frogeye.cuda {
    nix.settings = {
      substituters = [
        "https://cuda-maintainers.cachix.org"
      ];
      trusted-public-keys = [
        "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
      ];
    };
    nixpkgs.overlays = [
      (
        self: super:
        let
          pkgs_uf = import nixpkgs {
            inherit (super) system;
            config.allowUnfree = true;
          };
        in
        {
          ctranslate2 = (
            pkgs_uf.ctranslate2.override {
              withCUDA = true;
              withCuDNN = true;
            }
          );
        }
      )
    ];
  };
}
