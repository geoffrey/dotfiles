{
  ...
}:
{
  config = {
    services.dnsmasq = {
      # We want to be able to have two VPNs active at once.
      # Not an issue for routing, but we need local DNS with conditional forwarding.
      enable = true;
      resolveLocalQueries = true;
    };
  };
}
